/*
         MRCE Init.lua Script

                by Ashi

    Files are loaded from top to bottom.
    Any typos in paths or names will cause it to
    throw an error and stop loading files.

    errors from scripts that were successfuly found 
    and loaded will not cause this behavior however.
*/

-- Performs a version check. Will only show a warning if subversion check fails.
if VERSION != 202 then -- Just in case 2.3 breaks 2.2 mods or something
    error("\
                               No Way? No Way!\
                 MRCE Requires SRB2 version 2.2.10 or higher.\
              You have version 2.3 or a version older than 2.2.0!\
           Grab the latest version of 2.2 from srb2.org to play MRCE!", 0) return
elseif SUBVERSION < 10 then
    error("No Way? No Way!\nSRB2 Version 2.2.10 or higher is required but was not found.\nLoad halted.", 0) return
end


for _, filename in ipairs{
    -- ALWAYS Load GlobalFreeslot.lua first!!
    "GlobalFreeslot.lua",

    -- custom save and unlock system
    "GameCode/customsavesystem/savefunctions.lua",
    "GameCode/dialogsystem.lua",
    //"GameCode/customsavesystem/unlockables.lua",

    -- Loading HUD and GUI scripts
    "HUD/MRCEHUD.lua",
    "HUD/ContLives.lua",
    "HUD/NewEmblemHUD.lua",
    "HUD/blackemerald.lua",
    "HUD/Intermission.lua",
    "HUD/intermission_net.lua",
    "HUD/titlecard-credits.lua",

    -- Load menu code
    "Menu/Title/title_animation.lua",
    "Menu/episode_select.lua",
    -- Extras
    /*WIP*/"Menu/Extras/making_of_mystic_realm.lua",
    /*WIP*/"Menu/Extras/credits.lua",

    -- Load all the gameplay changes
    "miscgameplay/BotCommands.lua",
    "miscgameplay/ExAi.lua",
    "miscgameplay/jumpleniency.lua",
    "miscgameplay/RecodedMomentum.lua",
    "miscgameplay/STF.lua",
    "miscgameplay/Character/ReboundDash.lua",
    "miscgameplay/Character/superfloat.lua",
    "miscgameplay/xians-misc-stuff.lua",
	
	--Second Quest
	--Relies on code in my misc script, so load it right after
    "GameCode/secondquest.lua",	

    -- Level Code
    "LevelSpecific/Emerald Stages/Mystic_Shrines.lua",
    "LevelSpecific/Emerald Stages/emeralds.lua",
    "LevelSpecific/UtilityMap/dontdraw.lua",
    "LevelSpecific/DecoScaling.lua",
    "LevelSpecific/Aerial Garden/ExitStage.lua",
    "LevelSpecific/Aerial Garden/portal.lua",
    "LevelSpecific/Aerial Garden/LightTemple.lua",
    "LevelSpecific/primordialabyss/skychange.lua",
	
	--Flame Rift
    "LevelSpecific/Flame Rift/torchkey.lua",
	
	--Midnight Freeze
    "LevelSpecific/midnightfreeze/freezingwater.lua",

    -- Sunken Plant
    "LevelSpecific/Sunken Plant/electricpipe.lua",

    -- Dimension Warp
    "LevelSpecific/dimensionwarp/doomsday.lua",
    "LevelSpecific/dimensionwarp/decay.lua",
    "LevelSpecific/dimensionwarp/reveriefly.lua",
    "LevelSpecific/dimensionwarp/HMScheat.lua",

    -- Primordial Abyss

    -- Objects
    "Objects/CAKE.lua",
    "Objects/Cryocrawla.lua",
    "Objects/forcerollsprings.lua",
    "Objects/KHZDeco.lua",
    "Objects/NewEmblems.lua",
    "Objects/slowgoop.lua",
    "Objects/capsule.lua",
    --"Objects/rock/COLL.lua",
    --"Objects/rock/ROCK.lua",
    "Objects/mfzicicles.lua",
    "Objects/superdiagspring.lua", --super purple spring
    "Objects/hundringbox.lua", --placeholder for character icons
    --"Objects/Slime.lua", -- Code is wonky
    "Objects/hangglider.lua",
    "Objects/agzdeco.lua",
    "Objects/agzdetail.lua",


    -- Enemies
    "Enemies/DerelictCrawla.lua",
    "Enemies/PAZ/angel.lua",
    --"Enemies/Goggola.lua",
    "Enemies/Goopla.lua",
    --"Enemies/iciclivore.lua",
    --"Enemies/Octo.lua", -- Code is wonky

    -- Better Hive Elementals
    "Enemies/betterhiveelem.lua",

    -- Bosses (Zone Order)
    "Bosses/EggDecker.lua",

    "Bosses/Egg Baller/Freeslot.lua",
    "Bosses/Egg Baller/Helpers.lua",
    "Bosses/Egg Baller/Boss.lua",

    "Bosses/EggFreezer.lua",
    "Bosses/EggBomber.lua",

    -- Egg Animus (MRZ)
    "Bosses/EggAnimus/ondeath.lua",

    -- Egg Animus (DWZ)
    "Bosses/DWZ/ondeath.lua",
    "Bosses/Egg Animus/DWZ/Freeslot.lua",
    "Bosses/Egg Animus/DWZ/BossPrototype.lua"
} do
    dofile(filename)
end
