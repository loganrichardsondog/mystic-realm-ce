freeslot(
"MT_URING",
"MT_FAKEEMERALD1",
"MT_EGGBALLER",
"MT_EGGFREEZER",
"MT_EGGEBOMBER",
"MT_FBOSS",
"MT_FBOSS2",
"MT_XBOSS",
"MT_EGGANIMUS",
"MT_EGGANIMUS_EX",
"S_LIGHTNING_STONE",
"S_FIRE_STONE",
"S_WATER_STONE",
"SPR_KYST",
"MT_SUPERSPARKLES",
"SPR_SUSK",
"S_SUPERSPARK",
"MT_SPECCY",
"SKINCOLOR_GALAXY",
"SKINCOLOR_MR_VOID",
"SKINCOLOR_MRCEHYPER1",
"SKINCOLOR_MRCEHYPER2",
"SKINCOLOR_BLANK",
"SKINCOLOR_SUPERSAPPHIRE1",
"SKINCOLOR_SUPERSAPPHIRE2",
"SKINCOLOR_SUPERBUBBLEGUM1",
"SKINCOLOR_SUPERBUBBLEGUM2",
"SKINCOLOR_SUPERMINT1",
"SKINCOLOR_SUPERMINT2",
"SKINCOLOR_SUPERRUBY1",
"SKINCOLOR_SUPERRUBY2",
"SKINCOLOR_SUPERWAVE1",
"SKINCOLOR_SUPERWAVE2",
"SKINCOLOR_SUPERCOPPER1",
"SKINCOLOR_SUPERCOPPER2",
"SKINCOLOR_SUPERAETHER1",
"SKINCOLOR_SUPERAETHER2",
"MT_SLOWGOOP"
)

sfxinfo[freeslot("sfx_marioe")].caption = "Correct Solution"
sfxinfo[freeslot("sfx_mrfly")].caption = "Flight"
local debug = 0
if mr_portalopener == nil then
	rawset(_G, "mr_portalopener", 0)
end

//Ring Attraction function based upon P_Attract from source.
local function SuperAttract(source, dest)
	local dist = 0
	local ndist = 0
	local speedmul = 0
	local tx = dest.x
	local ty = dest.y
	local tz = dest.z + (dest.height/2)
	local xydist = P_AproxDistance(tx - source.x, ty - source.y)
	if dest and dest.health and dest.valid and dest.type == MT_PLAYER
		source.angle = R_PointToAngle2(source.x, source.y, tx, ty)
		dist = P_AproxDistance(xydist, tz - source.z)
		if (dist < 1)
			dist = 1
		end
		speedmul = P_AproxDistance(dest.momx, dest.momy) + FixedMul(source.info.speed, source.scale)
		source.momx = FixedMul(FixedDiv(tx - source.x, dist), speedmul)
		source.momy = FixedMul(FixedDiv(ty - source.y, dist), speedmul)
		source.momz = FixedMul(FixedDiv(tz - source.z, dist), speedmul)
		ndist = P_AproxDistance(P_AproxDistance(tx - (source.x + source.momx), ty - (source.y+source.momy)), tz - (source.z+source.momz))
		if (ndist > dist)
			source.momx = 0
			source.momy = 0
			source.momz = 0
			P_TeleportMove(source, tx, ty, tz)
		end
	end
end

addHook("MobjThinker", function(ring)
    if ring and ring.valid and ring.health > 0
        local soup
        for p in players.iterate
            if p and p.valid and ((MRCE_isHyper(p)) or (mapheaderinfo[gamemap].lvlttl == "Dimension Warp"))  and R_PointToDist2(p.mo.x, p.mo.y, ring.x, ring.y) <= (RING_DIST/5) and abs(ring.z - p.mo.z) < (RING_DIST/5)
                soup = p.mo
            end
        end
        if soup
            local momRing = P_SpawnMobjFromMobj(ring, 0,0,0, MT_FLINGRING)
            momRing.followmo = soup
            P_RemoveMobj(ring)
        end
    end
end, MT_RING)

addHook("MobjThinker", function(ring)
    if ring and ring.valid and ring.health > 0 and ring.followmo and ring.followmo.valid
        if R_PointToDist2(ring.followmo.x, ring.followmo.y, ring.x, ring.y) <= RING_DIST/2 and abs(ring.z - ring.followmo.z) < RING_DIST/2
			SuperAttract(ring, ring.followmo)
        else
            ring.fuse = 5*TICRATE
        end
        if ring.fuse == 2
            P_SpawnMobjFromMobj(ring, 0,0,0, MT_RING)
            P_RemoveMobj(ring)
        end
    end
end, MT_FLINGRING)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 return true end --don't want players interacting with the emerald
end, MT_EMERALD1)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 return true end
end, MT_EMERALD2)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 return true end
end, MT_EMERALD3)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 return true end
end, MT_EMERALD4)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 return true end
end, MT_EMERALD5)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 return true end
end, MT_EMERALD6)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 return true end
end, MT_EMERALD7)

--goalring support for netgames
addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end

	if mobj.tracer.tracer  --tracers of tracers are dumb.
	and mobj.target
		if mobj.tracer.tracer.signglow == true
			mobj.tracer.tracer.blendmode = AST_ADD
		else
			mobj.tracer.tracer.blendmode = 0
		end
	end
	--if gamemap < 123 return end //only mudhole karst has a shrine rn, so only it needs this function
	
	--if gamemap > 129 return end
	
	if gamemap != 123 return end
	
	if GoalRing == nil
		P_RemoveMobj(mobj)
	end
end, MT_SIGN)

--thx inferno
--despawn ring maces in ultimate mode
addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end	 
	if ultimatemode and mobj.hprev
		if mobj.hprev.type == MT_RING and mobj.hprev.type == MT_CUSTOMMACEPOINT
			P_RemoveMobj(mobj)
		end
	end
end, MT_RING)


addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
    if mapheaderinfo[gamemap].lvlttl != "Dimension Warp" return end
	if netgame
	or not ultimatemode then
		P_RemoveMobj(mobj)
	end
end, MT_URING)

addHook("PlayerSpawn", function(p)
	if not p.realmo return false end
	if p.spectator return false end
	if p.mo.signglow == nil
		p.mo.signglow = false
	end
	if p.mrce == nil
		local mrce = {
		glowaura = 0,
		flycheat = false,
		hypercheat = false,
		canhyper = false,
		ultrastar = false,
		hyperimages = false,
		hypermode = 0,
		customskin = 0,
		dontwantphysics = false,
		physics = true,
		skipmystic = false,
		nasyamystic = false,
		exspark = false,
		ishyper = false,
		jump = 0,
		spin = 0,
		exsparkcolor = R_GetColorByName("Galaxy"),
		camroll = 0,
		cosmichysteria = false,
		speen = 0,
		freezeeffect = 0,
		hud = 1,
		constext = 0,
		forcehyper = 0
		}
		if p.mo
			p.mrce = mrce
		end
	end
	p.mrce.speen = 0
	p.mrce.constext = 8
	--p.pflags = $ & ~PF_GODMODE
	if io and p == consoleplayer and p.screenflash == nil
		local file = io.openlocal("client/mrce/hyperflash.dat")
		if file
			local string = file:read("*a")
			if string == "1" or string == "true" or string == "on"
				COM_BufInsertText(p, "hyperflash on")
			elseif string == "0" or string == "false" or string == "off" or string == nil
				COM_BufInsertText(p, "hyperflash off")
			end
			file:close()
		end
	end
	
	if io and p == consoleplayer
		local file = io.openlocal("client/mrce/hud.dat")
		if file
			local string = file:read("*a")
			if string == "1" or string == "on" or string == "default" or string == "normal" or string == "yes" or string == nil then
				COM_BufInsertText(p, "mr_hud 1")
			elseif string == "0" or string == "disable" or string == "off" or string == "no" then
				COM_BufInsertText(p, "mr_hud 0")
			end
			file:close()
		else
			COM_BufInsertText(p, "mr_hud 1")
		end
	end
--reset hyper values
	if p.mrce.forcehyper <= 0 then
		p.mrce.canhyper = false
		p.mrce.ultrastar = false
		p.mrce.hyperimages = false
	end
	p.skincheck = p.mo.skin
	
	if p.mo and p.mo.valid and p.mo.skin == "supersonic" and ((gamemap == 132)  or (gamemap == 133))
		P_GivePlayerRings(p, 50)
	end
end)

rawset(_G, "MRCE_isHyper", function(player)
	if not player.powers[pw_super] return false end
	local x = player.mrce
	if x and mrce_hyperunlocked then
		if x.canhyper
		or x.ultrastar
			return true
		end
	elseif x and x.hypercheat
	and (x.canhyper or x.ultrastar)
		return true
	else
		return false
	end
end)

//credit to katsy for this tiny function pulled from reveries. it's so simple I could've written it myself, but eh.
--Makes elemental shield not render its fire when in water. Because fire can't burn in water. Obviously.

addHook("MobjThinker", function(shield)
	if (shield.target) and shield.target.valid
		if (shield.target.type == MT_ELEMENTAL_ORB)
			if ((shield.target.target.eflags & MFE_UNDERWATER or shield.target.target.eflags & MFE_TOUCHWATER) and not (shield.target.target.eflags & MFE_TOUCHLAVA))
				shield.flags2 = $|MF2_DONTDRAW
			elseif (shield.flags2 & MF2_DONTDRAW)
				shield.flags2 = $ & ~MF2_DONTDRAW
			end
		end
	end
end, MT_OVERLAY)

//cheat codes. they're mostly here for debug purposes, but they're fun to mess with, so I'll likely be keeping them here.
//currently only 2 cheats are present; hyper cheat, which gives all 7 emeralds and unlocks hyper form, 
//and fly cheat, which allows super sonic to fly outside of dwz
//update, glow aura adds blendmode aura to sonic's rebound dash , jump ball, spindash, and roll. Was maybe thinking as a 100% complete reward, until then, it's an easter egg

COM_AddCommand("mrsecret", function(player, arg1, arg2)
	if (gamestate == GS_LEVEL) and player.valid
		if arg1 and arg1 == "4126"
			if not netgame
				player.mrce.hypercheat = true
				if modifiedgame == false
					COM_BufInsertText(player, "devmode 1")
					COM_BufInsertText(player, "devmode 0")
				end
				if arg2 and arg2 == "1" then
					player.mrce.forcehyper = 1
					player.mrce.canhyper = true
					player.mrce.ultrastar = false
				elseif arg2 and arg2 == "2" then
					player.mrce.forcehyper = 2
					player.mrce.canhyper = false
					player.mrce.ultrastar = true
				elseif arg2 and arg2 == "0" then
					player.mrce.forcehyper = 0
				end
			else
				print(player.name .. " is a sussy baka")
				P_DamageMobj(player.mo,nil,nil,1,DMG_INSTAKILL)
			end
		elseif arg1 == "20071101" and (player == consoleplayer) and not netgame
			player.mrce.flycheat = true
		elseif arg1 == "0" or arg1 == "off"
			player.mrce.flycheat = false
			player.mrce.hypercheat = false
			player.mrce.exspark = false
			player.mrce.glowaura = 0
			player.mrce.forcehyper = 0
			if player.pet
				player.mo.pet.blendmode = 0
				player.mo.pet.colorized = false
			end
			if player.followmobj then
				player.followmobj.blendmode = 0
				player.followmobj.colorized = false
			end
			if player.buddies
				for id,buddy in pairs(player.buddies) do
					if buddy.mo and buddy.mo.valid
						buddy.mo.blendmode = 0
						buddy.mo.colorized = false
					end
				end
			end
		elseif arg1 == "20100523"
			if player.mrce.exspark
				player.mrce.exspark = false
			else
				player.mrce.exspark = true
				if arg2 and R_GetColorByName(arg2) then
					player.mrce.exsparkcolor = R_GetColorByName(arg2)
				else
					player.mrce.exsparkcolor = player.mo.color
				end
			end
		elseif player.mrce.exspark == true and arg1 and (arg1 == "sparkcolor" or arg1 == "66279") and arg2 and R_GetColorByName(arg2) then
			player.mrce.exsparkcolor = R_GetColorByName(arg2)
		elseif arg1 == "1207" or arg1 == "glow"
			if player.mrce.glowaura > 0 and not arg2
				player.mrce.glowaura = 0
				player.mo.colorized = false
				player.mo.blendmode = 0
				if player.pet
					player.mo.pet.blendmode = 0
					player.mo.pet.colorized = false
				end
				if player.followmobj then
					player.followmobj.blendmode = 0
					player.followmobj.colorized = false
				end
				if player.buddies
					for id,buddy in pairs(player.buddies) do
						if buddy.mo and buddy.mo.valid
							buddy.mo.blendmode = 0
							buddy.mo.colorized = false
						end
					end
				end
			else
				if player.spinitem == MT_THOK
					player.spinitem = 0
				end
				if not arg2
				or arg2 == "1" or arg2 == "add" then
					player.mrce.glowaura = 1
					if player.pet
						player.mo.pet.blendmode = AST_ADD
						if not (player.mo.pet.color == SKINCOLOR_NONE)
							player.mo.pet.colorized = true
						end
					end
				elseif arg2 and (arg2 == "2" or arg2 == "subtract") then
					player.mrce.glowaura = 2
					if player.pet
						player.mo.pet.blendmode = AST_SUBTRACT
						if not (player.mo.pet.color == SKINCOLOR_NONE)
							player.mo.pet.colorized = true
						end
					end
				end
			end
		else
			CONS_Printf(player, "\133invalid input")
		end
	end
end, 0)
if MRCE_superSpark == nil then
	rawset(_G, "MRCE_superSpark", function(mo, amount, fuse, speed1, speed2, hyper, usecolor)
		local colors = {SKINCOLOR_EMERALD, SKINCOLOR_PURPLE, SKINCOLOR_BLUE, SKINCOLOR_CYAN, SKINCOLOR_ORANGE, SKINCOLOR_RED, SKINCOLOR_GREY}
		if not (mo and mo.valid and amount ~= nil and fuse ~= nil and speed1 ~= nil and speed2 ~= nil and hyper ~= nil) then
			return
		end

		for i = 0, amount, 1 do
			local ha = P_RandomRange(0, 360)*ANG1
			local va = P_RandomRange(0, 360)*ANG1
			local speed = P_RandomRange(speed1/FRACUNIT, speed2/FRACUNIT)*FRACUNIT

			local sprk = P_SpawnMobj(mo.x + FixedMul(FixedMul(mo.radius, FixedMul(cos(ha), cos(va))), mo.scale),
									 mo.y + FixedMul(FixedMul(mo.radius, FixedMul(sin(ha), cos(va))), mo.scale),
									 mo.z + FixedMul(FixedMul(mo.radius, sin(va)), mo.scale) + FixedMul(mo.scale, mo.height/2),
									 MT_SUPERSPARKLES)

			sprk.scale = mo.scale
			sprk.fuse = fuse
			if hyper == false then
				if usecolor == 0 then
					if mo.color then
						sprk.color = mo.color
					else
						sprk.color = 60
					end
				else
					sprk.color = usecolor
				end
			else
				sprk.color = colors[P_RandomRange(1, #colors)]
			end
			if mo.player and not camera.chase then
				sprk.flags2 = $|MF2_DONTDRAW
			end

			sprk.momx = FixedMul(FixedMul(speed, FixedMul(cos(ha), cos(va))), mo.scale)
			sprk.momy = FixedMul(FixedMul(speed, FixedMul(sin(ha), cos(va))), mo.scale)
			sprk.momz = FixedMul(FixedMul(speed, sin(va)), mo.scale)
		end
	end)
end
addHook("MobjThinker", function(mobj)
    if not (mobj and mobj.valid) return end
//	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
//		MRCE_superSpark(mobj, 1, 5, 1, 21845, false, 55)
//	end
	if mobj.state >= S_FAKEEMERALD1 and mobj.state <= S_FAKEEMERALD8 then
		mobj.blendmode = AST_ADD
	end
	if (emeralds & EMERALD1) != 1 and mobj.state == S_FAKEEMERALD1 then
		P_RemoveMobj(mobj)
	end
	if (emeralds & EMERALD2) != 2 and mobj.state == S_FAKEEMERALD2 then
		P_RemoveMobj(mobj)
	end
	if (emeralds & EMERALD3) != 4 and mobj.state == S_FAKEEMERALD3 then
		P_RemoveMobj(mobj)
	end
	if (emeralds & EMERALD4) != 8 and mobj.state == S_FAKEEMERALD4 then
		P_RemoveMobj(mobj)
	end
	if (emeralds & EMERALD5) != 16 and mobj.state == S_FAKEEMERALD5 then
		P_RemoveMobj(mobj)
	end
	if (emeralds & EMERALD6) != 32 and mobj.state == S_FAKEEMERALD6 then
		P_RemoveMobj(mobj)
	end
	if (emeralds & EMERALD7) != 64 and mobj.state == S_FAKEEMERALD7 then
		P_RemoveMobj(mobj)
	end
	if not mrce_hyperunlocked and mobj.state == S_FAKEEMERALD8 then
		P_RemoveMobj(mobj)
	end
	if gamemap == 121 then
		if mobj.portalslide and mr_portalopener and mr_portalopener >= 7 then
			mobj.flags = MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_NOCLIPHEIGHT
			if mobj.state == S_PORTALSCRAP1 then
				P_InstaThrust(mobj, ANGLE_90, 70*FRACUNIT)
				mobj.rollangle = $ + ANG2
				mobj.angle = ANGLE_90
			else
				P_InstaThrust(mobj, ANGLE_90, 80*FRACUNIT)
				mobj.rollangle = $ + ANG10
			end
			if mobj.y > 12709*FRACUNIT and mobj.valid then
				P_KillMobj(mobj)
			end
		end
		if mobj.valid and mobj.portalpull and mr_portalopener >= 7 then
			mobj.flags = MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_NOCLIPHEIGHT
			P_Thrust(mobj, (R_PointToAngle2(mobj.x, mobj.y, 0, 320*FRACUNIT)), FRACUNIT)
			if mobj.x > -768*FRACUNIT
			and mobj.y > 256*FRACUNIT
			and mobj.x < 768*FRACUNIT
			and mobj.y < 384*FRACUNIT then
				P_RemoveMobj(mobj)
			end
		end
	end
end, MT_FAKEEMERALD1)

addHook("MapLoad", function(p, v)
	if gamemap != 130 then return end
	if (emeralds & EMERALD1) == 1 then
		local em1 = P_SpawnMobj(8466*FRACUNIT, -469*FRACUNIT, 48*FRACUNIT, MT_FAKEEMERALD1)
		em1.state = S_FAKEEMERALD1
	end
	if (emeralds & EMERALD2) == 2 then
		local em2 = P_SpawnMobj(8818*FRACUNIT, -469*FRACUNIT, 48*FRACUNIT, MT_FAKEEMERALD1)
		em2.state = S_FAKEEMERALD2
	end
	if (emeralds & EMERALD3) == 4 then
		local em3 = P_SpawnMobj(8562*FRACUNIT, -437*FRACUNIT, 72*FRACUNIT, MT_FAKEEMERALD1)
		em3.state = S_FAKEEMERALD3
	end
	if (emeralds & EMERALD4) == 8 then
		local em4 = P_SpawnMobj(8722*FRACUNIT, -437*FRACUNIT, 72*FRACUNIT, MT_FAKEEMERALD1)
		em4.state = S_FAKEEMERALD4
	end
	if (emeralds & EMERALD5) == 16 then
		local em5 = P_SpawnMobj(8498*FRACUNIT, -405*FRACUNIT, 88*FRACUNIT, MT_FAKEEMERALD1)
		em5.state = S_FAKEEMERALD5
	end
	if (emeralds & EMERALD6) == 32 then
		local em6 = P_SpawnMobj(8786*FRACUNIT, -405*FRACUNIT, 88*FRACUNIT, MT_FAKEEMERALD1)
		em6.state = S_FAKEEMERALD6
	end
	if (emeralds & EMERALD7) == 64 then
		local em7 = P_SpawnMobj(8642*FRACUNIT, -421*FRACUNIT, 104*FRACUNIT, MT_FAKEEMERALD1)
		em7.state = S_FAKEEMERALD7
	end
end)

addHook("MobjThinker", function(mobj)
    if not (mobj and mobj.valid) return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 55)
	end
	if mobj.valid
		mobj.blendmode = AST_ADD
	end
end, MT_EMERALD1)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 62)
	end
	if mobj.valid
		mobj.blendmode = AST_ADD
	end
	if gamemap == 124 then
		if GoalRing == nil then
			P_RemoveMobj(mobj)
		end
	end
end, MT_EMERALD2)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 43)
	end
	if mobj.valid
		mobj.blendmode = AST_ADD
	end
	if gamemap == 125 then
		if GoalRing == nil then
			P_RemoveMobj(mobj)
		end
	end
end, MT_EMERALD3)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 23)
	end
	if mobj.valid
		mobj.blendmode = AST_ADD
	end
	if gamemap == 126 then
		if GoalRing == nil then
			P_RemoveMobj(mobj)
		end
	end
end, MT_EMERALD4)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 50)
	end
	if mobj.valid
		mobj.blendmode = AST_ADD
	end
	if gamemap == 127 then
		if GoalRing == nil then
			P_RemoveMobj(mobj)
		end
	end
end, MT_EMERALD5)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 36)
	end
	if mobj.valid
		mobj.blendmode = AST_ADD
	end
	if gamemap == 128 then
		if GoalRing == nil
			P_RemoveMobj(mobj)
		end
	end
end, MT_EMERALD6)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 3)
	end
	if mobj.valid
		mobj.blendmode = AST_ADD
	end
end, MT_EMERALD7)

--air bubble thinker
addHook("TouchSpecial", function(mo, toucher)
	if toucher.player
	and MRCE_isHyper(toucher.player)
		return true -- hyper forms don't need bubbles
	elseif (toucher.player.yusonictable and toucher.player.yusonictable.hypersonic and toucher.player.mo.skin == "adventuresonic") then return true
	else toucher.player.powers[pw_spacetime] = 12 * TICRATE end --midnight freeze's ice water uses space countdown
end, MT_EXTRALARGEBUBBLE)

--break spikes
local function Break_shit (plr, thing)
	if (thing.type == MT_SPIKE
	or thing.type == MT_WALLSPIKE)
	then
		if (thing.flags & MF_SOLID) and ((thing.type == MT_SPIKE) or (thing.type == MT_WALLSPIKE))
		then
			S_StartSound(plr, thing.info.deathsound)
		end
		for iter in thing.subsector.sector.thinglist()
		do
			if (
				iter.type == thing.type and
				iter.health > 0 and
				( iter.flags & MF_SOLID ) and
				(iter == thing or
				P_AproxDistance(P_AproxDistance(
				thing.x - iter.x, thing.y - iter.y),
				thing.z - iter.z) < 56*thing.scale))
			then
				P_KillMobj(iter, plr, tmthing, 0)
			end
		end
		return 2
		elseif (( thing.flags & MF_MONITOR ))
		then
		if (P_DamageMobj(thing, tmthing, tmthing, 1, 0))
		then
			return 2
		end
	end
end

//break shit with hyper form
addHook ("MobjMoveCollide", function (plr, thing)
	if thing and thing.valid
		if plr.player ~= nil and MRCE_isHyper(plr.player)
			if (thing.type == MT_SPIKE
			or thing.type == MT_WALLSPIKE)
				if (plr.z + plr.height < thing.z) 
				or (plr.z > thing.z + thing.height)
					return false
				else
					return Break_shit(plr, thing)
				end
			end
			if (thing.flags & MF_MONITOR)
			and not (plr.player.ctfteam == 1 and thing.type == MT_RING_BLUEBOX)
			and not (plr.player.ctfteam == 2 and thing.type == MT_RING_REDBOX)
			and not (plr.z > thing.z+thing.height+(10*FRACUNIT)) and not (thing.z > plr.z+plr.height+(10*FRACUNIT))
				P_KillMobj(thing, plr, plr)
				return false
			end
			if thing.type == MT_TURRET
			or thing.type == MT_EGGROBO1 then
				if (plr.z + plr.height < thing.z) 
				or (plr.z > thing.z + thing.height)
					return false
				else
					P_KillMobj(thing, plr, plr)
					S_StartSound(thing, sfx_bedeen)
					thing.state = S_NULL
					return false
				end
			end
		end
	end
end, MT_PLAYER)

//Wow this mod's been passed around a lot. Originally based on hyper abilities v4.1.2, then edited for mrce. 
//It barely maintains any of the original code, but credit is still due.  v4.1.2 by GameBoyTM101
//Credits for the original scripts from 2.1 go to MotdSpork on the SRB2 Message Board, as well as HitKid61/HitCoder
//Additional credits to Radicalicious for the Multiglide Knuckles and Infinite Flight Tails code.
//Also credit to DirkTheHusky for the custom colors themed after the 7 emeralds

rawset(_G, "mrce_hyperunlocked", false) --this is here for mrce to detect if hyper has been unlocked
rawset(_G, "mrce_secondquest", false) --sets whether second quest is currently active
local mrce_dowarptime = false --check if the post AGZ4 return warp is active
rawset(_G, "mrce_hyperstones", 0)

mobjinfo[MT_LIGHTNING_STONE] = {
	doomednum = 3110,
	spawnstate = S_LIGHTNING_STONE,
	radius = 28*FRACUNIT,
	height = 38*FRACUNIT,
	flags = MF_NOGRAVITY|MF_SPECIAL
}

states[S_LIGHTNING_STONE] = {
	sprite = SPR_KYST,
	frame = A,
}

addHook("TouchSpecial", function(mo, toucher)
	if mo and mo.valid then
		if mapheaderinfo[gamemap].lvlttl != "Starlight Palace" then
			if not (mrce_hyperstones & (1 << (0))) then
				mrce_hyperstones = $ | (1 << (0))
				S_StartSound(null, sfx_cdfm63)
			end
		else
			return true
		end
	end
end, MT_LIGHTNING_STONE)

addHook("MobjThinker", function(mo)
	if mo and mo.valid then
		if leveltime > 3 then
			if (mrce_hyperstones & (1 << (0)))
			and mapheaderinfo[gamemap].lvlttl != "Starlight Palace" then
				P_RemoveMobj(mo)
			elseif mapheaderinfo[gamemap].lvlttl == "Starlight Palace"
			and not (mrce_hyperstones & (1 << (0))) then
				P_RemoveMobj(mo)
			end
		end
	end
end, MT_LIGHTNING_STONE)

mobjinfo[MT_FIRE_STONE] = {
	doomednum = 3111,
	spawnstate = S_FIRE_STONE,
	radius = 28*FRACUNIT,
	height = 38*FRACUNIT,
	flags = MF_NOGRAVITY|MF_SPECIAL
}

states[S_FIRE_STONE] = {
	sprite = SPR_KYST,
	frame = B,
}

addHook("TouchSpecial", function(mo, toucher)
	if mo and mo.valid then
		if mapheaderinfo[gamemap].lvlttl != "Starlight Palace" then
			if not (mrce_hyperstones & (1 << (1))) then
				mrce_hyperstones = $ | (1 << (1))
				S_StartSound(null, sfx_cdfm63)
			end
		else
			return true
		end
	end
end, MT_FIRE_STONE)

addHook("MobjThinker", function(mo)
	if mo and mo.valid then
		if leveltime > 3 then
			if (mrce_hyperstones & (1 << (1)))
			and mapheaderinfo[gamemap].lvlttl != "Starlight Palace" then
				P_RemoveMobj(mo)
			elseif mapheaderinfo[gamemap].lvlttl == "Starlight Palace"
			and not (mrce_hyperstones & (1 << (1))) then
				P_RemoveMobj(mo)
			end
		end
	end
end, MT_FIRE_STONE)

mobjinfo[MT_WATER_STONE] = {
	doomednum = 3112,
	spawnstate = S_WATER_STONE,
	radius = 28*FRACUNIT,
	height = 38*FRACUNIT,
	flags = MF_NOGRAVITY|MF_SPECIAL
}

states[S_WATER_STONE] = {
	sprite = SPR_KYST,
	frame = C,
}

addHook("TouchSpecial", function(mo, toucher)
	if mo and mo.valid then
		if mapheaderinfo[gamemap].lvlttl != "Starlight Palace" then
			if not (mrce_hyperstones & (1 << (2))) then
				mrce_hyperstones = $ | (1 << (2))
				S_StartSound(null, sfx_cdfm63)
			end
		else
			return true
		end
	end
end, MT_WATER_STONE)

addHook("MobjThinker", function(mo)
	if mo and mo.valid then
		if leveltime > 3 then
			if (mrce_hyperstones & (1 << (2)))
			and mapheaderinfo[gamemap].lvlttl != "Starlight Palace" then
				P_RemoveMobj(mo)
			elseif mapheaderinfo[gamemap].lvlttl == "Starlight Palace"
			and not (mrce_hyperstones & (1 << (2))) then
				P_RemoveMobj(mo)
			end
		end
	end
end, MT_WATER_STONE)

local luabanks = reserveLuabanks() --save shit to the current savefile, not globally

skincolors[SKINCOLOR_GALAXY] = {
	name = "Galaxy",
	ramp = {178,162,163,165,166,167,167,157,158,158,159,253,253,30,30,31},
	invcolor = SKINCOLOR_SUNSET,
	invshade = 4,
	chatcolor = V_PURPLEMAP,
	accessible = true
}

skincolors[SKINCOLOR_MR_VOID] = {
	name = "Dark Prism",
	ramp = {177,179,181,182,183,166,166,167,167,168,168,169,253,254,254,30},
	invcolor = SKINCOLOR_SUNSET,
	invshade = 3,
	chatcolor = V_MAGENTAMAP,
	accessible = true
}

skincolors[SKINCOLOR_BLANK] = {
    name = "Blank",
    ramp = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
    accessible = false
}

skincolors[SKINCOLOR_SUPERSAPPHIRE1] = {
	name = "Super Sapphire 1",
	ramp = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 128, 131, 134, 135, 149, 150},
	accessible = false
}

skincolors[SKINCOLOR_SUPERSAPPHIRE2] = {
    name = "Super Sapphire 2",
    ramp = {0, 0, 0, 0, 128, 128, 131, 131, 134, 134, 135, 135, 149, 149, 150, 150},
    accessible = false
}

skincolors[SKINCOLOR_SUPERBUBBLEGUM1] = {
	name = "Super Bubblegum 1",
	ramp = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 208, 200, 178, 179, 180, 181},
	accessible = false
}

skincolors[SKINCOLOR_SUPERBUBBLEGUM2] = {
    name = "Super Bubblegum 2",
    ramp = {0, 0, 0, 0, 208, 208, 200, 200, 178, 178, 179, 179, 180, 180, 181, 181},
    accessible = false
}

skincolors[SKINCOLOR_SUPERMINT1] = {
	name = "Super Mint 1",
	ramp = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 88, 89, 98, 99, 100, 101},
	accessible = false
}

skincolors[SKINCOLOR_SUPERMINT2] = {
    name = "Super Mint 2",
    ramp = {0, 0, 0, 0, 88, 88, 89, 89, 98, 98, 99, 99, 100, 100, 101, 101},
    accessible = false
}

skincolors[SKINCOLOR_SUPERRUBY1] = {
	name = "Super Ruby 1",
	ramp = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 176, 201, 202, 203, 204, 38},
	accessible = false
}

skincolors[SKINCOLOR_SUPERRUBY2] = {
    name = "Super Ruby 2",
    ramp = {0, 0, 0, 0, 176, 176, 201, 201, 202, 202, 203, 203, 204, 204, 38, 38},
    accessible = false
}

skincolors[SKINCOLOR_SUPERWAVE1] = {
	name = "Super Wave 1",
	ramp = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 120, 121, 141, 135, 136, 137},
	accessible = false
}

skincolors[SKINCOLOR_SUPERWAVE2] = {
    name = "Super Wave 2",
    ramp = {0, 0, 0, 0, 120, 120, 121, 121, 141, 141, 135, 135, 136, 136, 137, 137},
    accessible = false
}

skincolors[SKINCOLOR_SUPERCOPPER1] = {
	name = "Super Copper 1",
	ramp = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 81, 82, 64, 52, 53, 54},
	accessible = false
}

skincolors[SKINCOLOR_SUPERCOPPER2] = {
    name = "Super Copper 2",
    ramp = {0, 0, 0, 0, 81, 81, 82, 82, 64, 64, 52, 52, 53, 53, 54, 54},
    accessible = false
}

skincolors[SKINCOLOR_SUPERAETHER1] = {
	name = "Super Aether 1",
	ramp = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 145, 170},
	accessible = false
}

skincolors[SKINCOLOR_SUPERAETHER2] = {
    name = "Super Aether 2",
    ramp = {0, 0, 0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 145, 145, 170, 170},
    accessible = false
}

skincolors[SKINCOLOR_MRCEHYPER1] = {
	name = "Hyper1",
	ramp = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
	accessible = false
}

skincolors[SKINCOLOR_MRCEHYPER2] = {
	name = "Hyper2",
	ramp = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
	accessible = false
}


local flashColor1 = skincolors[SKINCOLOR_MRCEHYPER1]
local flashColor2 = skincolors[SKINCOLOR_MRCEHYPER2]
local flashDelay = 4 //change this to how many tics it takes to animate
local colorpos1 = 1
local colorpos2 = 1
local colors1 = {
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 128, 131, 134, 135, 149, 150},
{0, 0, 0, 0, 128, 128, 131, 131, 134, 134, 135, 135, 149, 149, 150, 150},  --blue
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 128, 131, 134, 135, 149, 150},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 208, 200, 178, 179, 180, 181},
{0, 0, 0, 0, 208, 208, 200, 200, 178, 178, 179, 179, 180, 180, 181, 181},  --purple
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 208, 200, 178, 179, 180, 181},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 88, 89, 98, 99, 100, 101},
{0, 0, 0, 0, 88, 88, 89, 89, 98, 98, 99, 99, 100, 100, 101, 101},  --green
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 88, 89, 98, 99, 100, 101},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 176, 201, 202, 203, 204, 38},
{0, 0, 0, 0, 176, 176, 201, 201, 202, 202, 203, 203, 204, 204, 38, 38},  --red
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 176, 201, 202, 203, 204, 38},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 120, 121, 141, 135, 136, 137},
{0, 0, 0, 0, 120, 120, 121, 121, 141, 141, 135, 135, 136, 136, 137, 137},  --cyan
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 120, 121, 141, 135, 136, 137},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 81, 82, 64, 52, 53, 54},
{0, 0, 0, 0, 81, 81, 82, 82, 64, 64, 52, 52, 53, 53, 54, 54},  --yellow
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 81, 82, 64, 52, 53, 54},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 145, 170},
{0, 0, 0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 145, 145, 170, 170},  --white
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 145, 170},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}}

local colors2 = {
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 128, 131, 134, 135, 149, 150},
{0, 0, 0, 0, 128, 128, 131, 131, 134, 134, 135, 135, 149, 149, 150, 150},
{0, 0, 128, 128, 131, 131, 134, 134, 135, 135, 149, 149, 150, 150, 151, 151},
{0, 128, 131, 134, 135, 149, 150, 151, 152, 153, 155, 156, 157, 158, 159, 253},
{128, 131, 134, 135, 149, 150, 151, 152, 153, 155, 156, 157, 158, 159, 253, 254}, --blue
{0, 128, 131, 134, 135, 149, 150, 151, 152, 153, 155, 156, 157, 158, 159, 253},
{0, 0, 128, 128, 131, 131, 134, 134, 135, 135, 149, 149, 150, 150, 151, 151},
{0, 0, 0, 0, 128, 128, 131, 131, 134, 134, 135, 135, 149, 149, 150, 150},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 128, 131, 134, 135, 149, 150},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 208, 200, 178, 179, 180, 181},
{0, 0, 0, 0, 208, 208, 200, 200, 178, 178, 179, 179, 180, 180, 181, 181},
{0, 0, 208, 208, 200, 200, 178, 178, 179, 179, 180, 180, 181, 181, 182, 182},
{0, 208, 200, 178, 179, 180, 181, 182, 163, 163, 164, 164, 165, 165, 166, 167},
{208, 200, 178, 179, 180, 181, 182, 163, 163, 164, 164, 165, 165, 166, 167, 168},  --purple
{0, 208, 200, 178, 179, 180, 181, 182, 163, 163, 164, 164, 165, 165, 166, 167},
{0, 0, 208, 208, 200, 200, 178, 178, 179, 179, 180, 180, 181, 181, 182, 182},
{0, 0, 0, 0, 208, 208, 200, 200, 178, 178, 179, 179, 180, 180, 181, 181},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 208, 200, 178, 179, 180, 181},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 88, 89, 98, 99, 100, 101},
{0, 0, 0, 0, 88, 88, 89, 89, 98, 98, 99, 99, 100, 100, 101, 101},
{0, 0, 88, 88, 89, 89, 98, 98, 99, 99, 100, 100, 101, 101, 102, 102},
{0, 88, 89, 98, 99, 100, 101, 102, 103, 103, 126, 126, 143, 143, 138, 138},
{88, 89, 98, 99, 100, 101, 102, 103, 103, 126, 126, 143, 143, 138, 138, 253},  --green
{0, 88, 89, 98, 99, 100, 101, 102, 103, 103, 126, 126, 143, 143, 138, 138},
{0, 0, 88, 88, 89, 89, 98, 98, 99, 99, 100, 100, 101, 101, 102, 102},
{0, 0, 0, 0, 88, 88, 89, 89, 98, 98, 99, 99, 100, 100, 101, 101},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 88, 89, 98, 99, 100, 101},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 176, 201, 202, 203, 204, 38},
{0, 0, 0, 0, 176, 176, 201, 201, 202, 202, 203, 203, 204, 204, 38, 38},
{0, 0, 176, 176, 201, 201, 202, 202, 203, 203, 204, 204, 38, 38, 39, 39},
{0, 176, 201, 202, 203, 204, 38, 39, 40, 40, 41, 42, 185, 186, 187, 253},
{176, 201, 202, 203, 204, 38, 39, 40, 40, 41, 42, 185, 186, 187, 253, 254},  --red
{0, 176, 201, 202, 203, 204, 38, 39, 40, 40, 41, 42, 185, 186, 187, 253},
{0, 0, 176, 176, 201, 201, 202, 202, 203, 203, 204, 204, 38, 38, 39, 39},
{0, 0, 0, 0, 176, 176, 201, 201, 202, 202, 203, 203, 204, 204, 38, 38},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 176, 201, 202, 203, 204, 38},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 120, 121, 141, 135, 136, 137},
{0, 0, 0, 0, 120, 120, 121, 121, 141, 141, 135, 135, 136, 136, 137, 137},
{0, 0, 120, 120, 121, 121, 141, 141, 135, 135, 136, 136, 137, 137, 137, 137},
{0, 120, 121, 141, 135, 136, 137, 137, 174, 174, 168, 168, 169, 169, 159, 253},
{120, 121, 141, 135, 136, 137, 137, 174, 174, 168, 168, 169, 169, 253, 253, 254},  --cyan
{0, 120, 121, 141, 135, 136, 137, 137, 174, 174, 168, 168, 169, 169, 159, 253},
{0, 0, 120, 120, 121, 121, 141, 141, 135, 135, 136, 136, 137, 137, 137, 137},
{0, 0, 0, 0, 120, 120, 121, 121, 141, 141, 135, 135, 136, 136, 137, 137},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 120, 121, 141, 135, 136, 137},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 81, 82, 64, 52, 53, 54},
{0, 0, 0, 0, 81, 81, 82, 82, 64, 64, 52, 52, 53, 53, 54, 54},
{0, 0, 81, 81, 82, 82, 64, 64, 52, 52, 53, 53, 54, 54, 55, 55},
{0, 81, 82, 64, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 42, 43},
{81, 82, 64, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 42, 43, 44},  --yellow
{0, 81, 82, 64, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 42, 43},
{0, 0, 81, 81, 82, 82, 64, 64, 52, 52, 53, 53, 54, 54, 55, 55},
{0, 0, 0, 0, 81, 81, 82, 82, 64, 64, 52, 52, 53, 53, 54, 54},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 81, 82, 64, 52, 53, 54},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 145, 170},
{0, 0, 0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 145, 145, 170, 170},
{0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 145, 145, 170, 170, 171, 171},
{0, 0, 1, 2, 3, 145, 170, 171, 171, 172, 173, 173, 174, 175, 168, 168},
{0, 1, 2, 3, 145, 170, 171, 171, 172, 173, 173, 174, 175, 168, 168, 169},  --white
{0, 0, 1, 2, 3, 145, 170, 171, 171, 172, 173, 173, 174, 175, 168, 168},
{0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 145, 145, 170, 170, 171, 171},
{0, 0, 0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 145, 145, 170, 170},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 145, 170}}

local function HyperA()
	if not (leveltime % flashDelay) then
	
		if colorpos1 < #colors1 then
			colorpos1 = $ + 1
		else
			colorpos1 = 1
		end
		if colors1 and colorpos1 and colors1[colorpos1] then
			flashColor1.ramp = colors1[colorpos1]
		end
		
		if colorpos2 <= #colors2 then
			colorpos2 = $ + 1
		else
			colorpos2 = 1
		end
		if colors2 and colorpos2 and colors2[colorpos2] then
			flashColor2.ramp = colors2[colorpos2]
		end
	end
end

addHook("ThinkFrame", HyperA)

//when you have sensitive eyes, this command allows to disable screen flashing
COM_AddCommand("hyperflash", function(player, arg)
	if arg
		if arg == "1" or arg == "true" or arg == "on"
			if io and player == consoleplayer
				local file = io.openlocal("client/mrce/hyperflash.dat", "w+")
				file:write(arg)
				file:close()
			end
			player.screenflash = true
			CONS_Printf(player, "Hyper form screen flashing has been enabled.")
		elseif arg == "0" or arg == "false" or arg == "off"
			if io and player == consoleplayer
				local file = io.openlocal("client/mrce/hyperflash.dat", "w+")
				file:write(arg)
				file:close()
			end	
			player.screenflash = false
			CONS_Printf(player, "Hyper form screen flashing has been disabled.")
		elseif player.screenflash
			CONS_Printf(player, "hyperflash [on/off] - Toggles screen flashing caused by hyper forms. Currently on.")
		else
			CONS_Printf(player, "hyperflash [on/off] - Toggles screen flashing caused by hyper forms. Currently off.")
		end
	elseif player.screenflash
		CONS_Printf(player, "hyperflash [on/off] - Toggles screen flashing caused by hyper forms. Currently on.")
	else
		CONS_Printf(player, "hyperflash [on/off] - Toggles screen flashing caused by hyper forms. Currently off.")
	end
end)

//handle luabanks shenanigans, also gives knux and tails super abilities
addHook("PreThinkFrame",do
    for p in players.iterate
        if not (p and p.mo and p.mo.valid) continue end
		if MRCE_isHyper(p) and p.mrce and p.mrce.canhyper and not p.mrce.ultrastar
		and p.playerstate != PST_DEAD then
			p.mo.color = SKINCOLOR_MRCEHYPER1
		elseif MRCE_isHyper(p) and p.mrce and p.mrce.ultrastar and not p.mrce.canhyper
		and p.playerstate != PST_DEAD then
			p.mo.color = SKINCOLOR_MRCEHYPER2
		end
        if not p.powers[pw_super] and p.mo.skin == "knuckles" and mrce_hyperunlocked == true then
            p.charflags = $ & ~SF_MULTIABILITY
        end
        if p.powers[pw_super] and p.mo.skin == "knuckles" and mrce_hyperunlocked == true then
            p.charflags = $1 | SF_MULTIABILITY
        end
        if p.powers[pw_super] and p.pflags & PF_THOKKED then
            local ca = p.charability
            if ca == CA_FLY or ca == CA_SWIM
                p.powers[pw_tailsfly] = 8*TICRATE
            end
        elseif p.powers[pw_super] then
            p.powers[pw_tailsfly] = 0
        end
		if ((gamemap == 130 and (p.pflags & PF_FINISHED)) and All7Emeralds(emeralds) and mrce_hyperunlocked == false) then
			S_StartSound(null, sfx_s3k9c)
			luabanks[0] = 1
			mrce_hyperunlocked = true
		end
		if luabanks[0] == 1 then
			mrce_hyperunlocked = true
			if p.hyper and not p.hyper.isunlocked
				p.hyper.isunlocked = true
			end
			if p.yusonictable and not p.yusonictable.hyperpower and p.mo.skin == "adventuresonic" and p.mo.health then
				p.yusonictable.hyperpower = true
			end
		else
			mrce_hyperunlocked = false
		end
		if gamemap == 122 and p.pflags & PF_FINISHED then --we're in agz's warp room, and are warping to another zone
			luabanks[1] = 1
			mrce_dowarptime = true --initialize warp memory
		end
		if luabanks[1] == 1 then
			mrce_dowarptime = true --remember warp memory on reloading the save
		else
			mrce_dowarptime = false
		end
		if gamemap == 132 then --we made it to prismatic angel
			luabanks[1] = 0
			mrce_dowarptime = false --reset the value
		end
		if mrce_secondquest == true then
			luabanks[2] = 1
		else
			luabanks[2] = 0
		end
		if luabanks[2] == 1 and mrce_secondquest != true then
			mrce_secondquest = true
		end
		if leveltime == 1 then
			mrce_hyperstones = luabanks[3]
		end
		if leveltime > 1 then
			luabanks[3] = mrce_hyperstones
		end
		if debug == 1 then
			print("Hyperstones: " .. luabanks[3])
		end
    end
end)

addHook("ThinkFrame", do
/*	local silveradded = false
	if silv_TKextra != nil and silveradded == false then
		silv_TKextra = {

			[MT_SLOWGOOP]		=	{
									noaim = true,
									noroll = true,
									mo.fuse = TICRATE*2
									},
							}
		silveradded = true
	end*/
	for player in players.iterate
		if not player.realmo return false end
		if player.spectator return false end
		
		if not (netgame or multiplayer) then
			if ((MARIO_SILVERFIRECOLOR  != nil) or (SetupYuSonic != nil) or (Blaze_MoveTail != nil)) --character's that I'm aware of that use MusicChange.
			and leveltime == TICRATE then																											--we can't read skindata since the problem occurs so long as that file is loaded
				COM_BufInsertText(player, "mr_intmus Off")							--MusicChange is a really cool hook. Unfortunately is has a very high crash rate when used, and will crash mrce's custom intermission 9/10 times
			end		 																																	--so we have to disable the music switch to prevent that
		end
		if (player.mo.eflags & MFE_VERTICALFLIP) and (player.pflags & PF_FLIPCAM)// and mapheaderinfo[gamemap].lvlttl == "Prismatic Angel" then
			player.pflags = $ & ~PF_FLIPCAM
		end
		if player.mo.skin == "supersonic"
			player.charability = 18
			if not player.mo.shoestats then
				player.actionspd = 50
			end
			if gamemap == 98 or gamemap == 99
				if not (leveltime%35)
				and not (player.bot)
				and not (player.mo.state >= S_PLAY_SUPER_TRANS1) and (player.mo.state <= S_PLAY_SUPER_TRANS6)
					P_GivePlayerRings(player, 1)
				end
			end
		end
		if player.mo and (player.mo.skin == "sonic" or player.mo.skin == "modernsonic")
			player.mrce.hyperimages = true
		end
		if player.mo and (player.mo.skin == "sonic" or player.mo.skin == "modernsonic" or player.mrce.canhyper == true)
			if player.powers[pw_super]
			
				if not player.mo.hyperflashcolor
					player.mo.hyperflashcolor = 0
				end
			
				player.mo.hyperflashcolor = $1+1
			
				if player.mo.hyperflashcolor > 59
					player.mo.hyperflashcolor = 1
				end
			end
		end
	end
end)

addHook("PostThinkFrame",do
	for p in players.iterate
		if not p.realmo continue end
		if p.spectator continue end
		if p.mrce.speen then
			p.drawangle = p.mrce.speen
		end
		if MRCE_isHyper(p) and p.mrce and p.mrce.canhyper and not p.mrce.ultrastar
			and p.playerstate != PST_DEAD then
			p.mo.color = SKINCOLOR_MRCEHYPER1
			local ghost = P_SpawnGhostMobj(p.mo)
			ghost.fuse = 1
			if p.mrce.glowaura < 2 then
				ghost.blendmode = AST_ADD
			end
			ghost.frame = $|FF_TRANS30
			if p.fakeroll then
				ghost.rollangle = p.fakeroll
			else
				ghost.rollangle = p.mo.rollangle
			end
			ghost.color = p.mo.color
			P_TeleportMove(ghost, p.mo.x, p.mo.y, p.mo.z)
			ghost.destscale = p.mo.scale * (3 / 2)
		elseif MRCE_isHyper(p) and p.mrce and p.mrce.ultrastar and not p.mrce.canhyper
		and p.playerstate != PST_DEAD then
			p.mo.color = SKINCOLOR_MRCEHYPER2
		end
	end
end)

addHook("ThinkFrame", do
	for player in players.iterate
		if not player.realmo continue end
		if player.spectator continue end
		if player.mo and (player.mo.skin == "sonic" or player.mo.skin == "modernsonic" or player.mrce.canhyper == true)
			if not (player.mrce.hypermode)
			or player.powers[pw_super] == 0
			or player.exiting
			or player.mo.health == 0
				player.mrce.hypermode = 0
			end
			if player.powers[pw_super]
			and player.mo.health
			and player.mrce.hypermode == 0
			and mrce_hyperunlocked == true
				player.pflags = $1|PF_THOKKED
				player.mrce.hypermode = 1
				S_StartSound(player.mo, sfx_supert)
			end			

			if MRCE_isHyper(player)
			and player.mrce.canhyper == true
			and player.playerstate != PST_DEAD
				player.powers[pw_underwater] = 0
				player.powers[pw_spacetime] = 0
				player.mo.color = SKINCOLOR_MRCEHYPER1

			end
		end
		if player.mo 
			if player.mrce.ultrastar == true and MRCE_isHyper(player)
				player.powers[pw_underwater] = 0
				player.powers[pw_spacetime] = 0
				player.mo.color = SKINCOLOR_MRCEHYPER2
			end
		end
	end
end)

--addHook("SpinSpecial", function(player)

--here it is. our really big playerthink hook
addHook("PlayerThink", function(p)
	if p.spectator return false end
	if not p.realmo return false end
	local x = p.mrce
--detect if physics are enabled
	if IsCustomSkin(p) 
		x.physics = false
	else
		x.physics = true
	end
	if x.constext > 0 then x.constext = $ - 1 end
	--print(p.powers[pw_extralife])
	if p.pflags & PF_FINISHED and p.mrce.glowaura then
		p.powers[pw_ignorelatch] = 32768
	end

	if mrce_hyperunlocked == true and p.yusonictable and p.yusonictable.supersonic and p.mo.skin == "adventuresonic" then
		p.yusonictable.hypersonic = true
	end
	
	if p.mo and p.mo.skin == "sonic" or p.mo.skin == "supersonic" then
		p.spinitem = 0
	end
--disable custom physics for a few known characters
	if p.mo and ((p.mo.skin == "sms") or (p.mo.skin == "ass") or (p.mo.skin == "juniosonic") or (p.mo.skin == "iclyn") or (p.mo.skin =="kiryu") or /*(p.mo.skin == "ryder") or */(p.mo.skin == "adventuresonic"))
		x.customskin = 2
		x.physics = false
	elseif x.dontwantphysics == false
		x.physics = true
	end
	
	if p.pet
		if gamemap == 121 and p.exiting and All7Emeralds(emeralds) then
			p.mo.pet.flags2 = $|MF2_DONTDRAW --pets support for pet to enter portal as well
		end
	end

--post AGZ4 return warp handler
	if (gamemap == 103 or gamemap == 106 or gamemap == 109 or gamemap == 112 or gamemap == 115 or gamemap == 118)
	and mrce_dowarptime == true then
		if debug == 1
			print("AGZ4 go time")
		end
		G_SetCustomExitVars(122,1)
	end
	
--in ultimate mode, take away all rings unless in dwz
	if p.rings > 0 and ultimatemode and (mapheaderinfo[gamemap].lvlttl != "Dimension Warp") then
		p.rings = 0
	end

--fall animations. bc fuck airwalk, all my homies hate airwalk
	
	--
	if not (p.mo.skin == "msonic")
	and not (p.moskin == "modernsonic")
	and not (p.mo.skin == "adventuresonic")
	and not (p.mo.skin == "nasya")
		if (p.mo.state == S_PLAY_FALL)
		and ((p.mo.momz*P_MobjFlip(p.mo)) > 2*FRACUNIT)
		and not (p.sms_sontransform)
		and not (p.mo.eflags & MFE_GOOWATER)
		and not (p.mo.player.powers[pw_carry])
		and not (p.mo.player.pflags & PF_SHIELDABILITY)
			p.mo.state = S_PLAY_SPRING
		end
		
		if ((p.mo.state == S_PLAY_STND) or (p.mo.state == S_PLAY_WAIT) or (p.mo.state == S_PLAY_WALK) or (p.mo.state == S_PLAY_RUN) or (p.mo.state == S_PLAY_DASH))
		and not P_IsObjectOnGround(p.mo)
		and not (p.mo.standingslope)
		and not (p.mo.player.powers[pw_carry])
		and not (p.mo.player.secondjump)
		and p.mo.player.playerstate == PST_LIVE
			if ((p.mo.momz*P_MobjFlip(p.mo)) > 1*FRACUNIT)
			and not (p.mo.eflags & MFE_GOOWATER)
				p.mo.state = S_PLAY_SPRING
			elseif ((p.mo.momz*P_MobjFlip(p.mo)) < -1*FRACUNIT)
				p.mo.state = S_PLAY_FALL
			end
		end
	end
	
--if p.speed == go fast, allow the player to run on water. aka, momentum based water running
	if (p.speed >= 60*FRACUNIT) and not (skins[p.mo.skin].flags & SF_RUNONWATER)  and not IsCustomSkin(p) 
	and not ((p.mo.skin == "mario" or p.mo.skin == "luigi") and p.powers[pw_shield] == SH_MINI) --mini mario gets to always run on water
		p.charflags = $1|SF_RUNONWATER
	elseif not IsCustomSkin(p)
		if not (skins[p.mo.skin].flags & SF_RUNONWATER)
		and not ((p.mo.skin == "mario" or p.mo.skin == "luigi") and p.powers[pw_shield] == SH_MINI)
		and p.speed <= 60*FRACUNIT
			p.charflags = $1 & ~(SF_RUNONWATER)
		end
	end
	
	x.canhyper = $ or false
	x.hyperimages = $ or false
	x.ultrastar = $ or false
	--super tails?
	if (p.mo.skin == "tails" or p.mo.skin == "amy" or p.mo.skin == "fang") and mrce_hyperunlocked == true
		p.charflags = $1 | SF_SUPER --yes
	elseif (p.mo.skin == "tails" or p.mo.skin == "amy" or p.mo.skin == "fang") and mrce_hyperunlocked == false
		p.charflags =  $ & ~SF_SUPER --nah
	end
	if p.powers[pw_super] and p.pflags & PF_THOKKED and p.mo.skin == "tails"
		p.powers[pw_tailsfly] = 8*TICRATE --super tails infinite fly
	end
	if p.powers[pw_super] and p.mo.skin == "tails"
		p.actionspd = skins[p.mo.skin].actionspd * 2
	elseif p.mo.skin == "tails"
		p.actionspd = skins[p.mo.skin].actionspd
	end
		
	--hyper modern sonic's boost fly doesn't drain additional rings unlike his super
	if MRCE_isHyper(p)
		if p.superboost == nil
			p.superboost = false
			p.boosting = false
		end
		if p.mo.skin == "modernsonic"
			if p.superboost == true
				if (leveltime % 5 == 0)             
				p.rings = p.rings + 1
				end
			end
			--also nukes because why the fuck not lol
			if (p.boosting == true or p.superboost == true) and (mapheaderinfo[gamemap].lvlttl != "Dimension Warp")
				if p.modernnuker == true
					P_NukeEnemies(p.mo, p.mo, 600*FRACUNIT)
					if p.screenflash == true
						P_FlashPal(p, PAL_WHITE, 7)
					end
					p.modernnuker = false
				end
			else
				p.modernnuker = true
			end
		end
	end
	
	--basic stat buffs for hyper form
	if MRCE_isHyper(p) and not p.slowgooptimer
		p.maxdash = skins[p.mo.skin].maxdash * 3 / 2
		p.jumpfactor = skins[p.mo.skin].jumpfactor * 6 / 5
		--p.supercolor = "Silver"  --doesn't actually work, the variable is read only. Maybe a request for 2.2.11?
		p.actionspd = skins[p.mo.skin].actionspd * 6 / 5
		p.mindash = skins[p.mo.skin].mindash * 5
		p.hyperstats = 1
	elseif not p.slowgooptimer
		p.maxdash = skins[p.mo.skin].maxdash
		p.jumpfactor = skins[p.mo.skin].jumpfactor
		--p.supercolor = "Gold"
		p.actionspd = skins[p.mo.skin].actionspd
		p.mindash = skins[p.mo.skin].mindash
		p.hyperstats = 0
	end
	
--reset hyper values if the player changes skin
	if p.skincheck != p.mo.skin
		x.canhyper = false
		x.ultrastar = false
		x.hyperimages = false
		p.skincheck = p.mo.skin
	end
	--if player has max lives and gets a 1up, grant rings instead
	/*if p.powers[pw_extralife] == 2 and p.lives == 99 then
		P_GivePlayerRings(p, 100)
	end*/
	if x.exspark != false and p.playerstate != PST_DEAD and not (p.exiting and gamemap == 121 and not netgame)
		if not MRCE_isHyper(p)
			MRCE_superSpark(p.mo, 1, 8, 1, 5*FRACUNIT, false, x.exsparkcolor)
		end
	end
--give the player rings if they break a invinc monitor while super. Make lemonade out of lemons.
	if p.powers[pw_super] and p.powers[pw_invulnerability] and p.mo.skin != "supersonic"
		P_GivePlayerRings(p, 20)
		S_StartSound(null, sfx_itemup)
		p.powers[pw_invulnerability] = 0
	end
	
	if x.ultrastar == true and p.scoreadd < 4 and MRCE_isHyper(p)
		p.scoreadd = 4
	elseif P_IsObjectOnGround(p.mo) and p.powers[pw_invulnerability] == 0
		p.scoreadd = 0
	end
	
--spawn the hyper sparkles
	if p and p.valid and p.mo and p.mo.valid
		and p.playerstate != PST_DEAD
		and MRCE_isHyper(p)
		and not (p.mo.state >= S_PLAY_SUPER_TRANS1 and p.mo.state <= S_PLAY_SUPER_TRANS5) then
		MRCE_superSpark(p.mo, 1, 16, 1, 6*FRACUNIT, true, nil)
	end

--compatibility checks, allowing custom characters to have hyper forms
	if p.mo and (p.mo.skin == "hms123311" or p.mo.skin == "fhms123311")
		if x.canhyper != nil
			x.canhyper = true
			x.hyperimages = true
		end
		if mrce_hyperunlocked == true
			p.pflags = $|PF_GODMODE
		else
			p.pflags = $ & ~PF_GODMODE
		end
	end
	local forwardmove = p.cmd.forwardmove
	--print("FUCK")
	if (p.mo.skin == "mario" or p.mo.skin == "luigi")
	and MRCE_isHyper(p)
		if p.mariocapeflight != 0 and not P_IsObjectOnGround(p.mo)
			if forwardmove <= -35
				p.mo.momz = -50*P_GetMobjGravity(p.mo)
				if p.startultrafly == true
					S_StartSound(p.mo, sfx_mrfly)
					p.startultrafly = false
				end
			else p.startultrafly = true
			end
		end
		if p.mariogroundpound and  P_IsObjectOnGround(p.mo) and p.startpoundnuke == true
			P_NukeEnemies(p.mo, p.mo, 900*FRACUNIT)
			p.startpoundnuke = false
			if p.screenflash == true
				P_FlashPal(p, PAL_WHITE, 7)
			end
		elseif not P_IsObjectOnGround(p.mo)
			p.startpoundnuke = true
		end
	end
		
	if p.mo.skin == "jana" and MRCE_isHyper(p)
		if p.jana.rockin and p.mo.frame & FF_FRAMEMASK == B
			P_Earthquake(p.mo, p.mo, 600*FRACUNIT)
		end
	end
	
	if x.forcehyper == 1 then
		x.canhyper = true
		x.ultrastar = false
	elseif x.forcehyper == 2 then
		x.ultrastar = true
		x.canhyper = false
	end

	if p.mo .skin == "tails"
	or p.mo.skin == "fang"
	or p.mo.skin == "amy"
	or p.mo.skin == "mcsonic"
	or p.mo.skin == "crystalsonic"
	or p.mo.skin == "altsonic"
	or p.mo.skin == "supersonic"
	and x.forcehyper <= 0 then
		x.hyperimages = false
		x.canhyper = false
		x.ultrastar = false
	end
	if p.mo.skin == "modernsonic"
	or p.mo.skin == "HMS123311"
	or p.mo.skin == "fhms123311"
	or p.mo.skin == "dirk"
	or p.mo.skin == "yoshi"
	or p.mo.skin == "sonic"
	or p.mo.skin == "pointy"
	or p.mo.skin == "shadow"
	or p.mo.skin == "flame"
	or p.mo.skin == "juniosonic"
	or p.mo.skin == "bandages"
	or p.mo.skin == "bean"
	or p.mo.skin == "greeneyesonic"
	or p.mo.skin == "cdsonic"
	or p.mo.skin == "xtreme"
	or p.mo.skin == "extralife"
	and x.forcehyper <= 0 then
		x.hyperimages = true
		x.canhyper = true
		x.ultrastar = false
	end
	if p.mo.skin == "metalsonic"
	or p.mo.skin == "knuckles"
	and x.forcehyper <= 0 then
		x.hyperimages = true
		x.ultrastar = false
		x.canhyper = false
	end
	if p.mo.skin == "mario"
	or p.mo.skin == "luigi"
	or p.mo.skin == "surge"
	or p.mo.skin == "skip"
	or p.mo.skin == "willow"
	or p.mo.skin == "silver"
	and x.forcehyper <= 0 then
		x.ultrastar = true
		x.hyperimages = false
		x.canhyper = false
	end
	if p.mo.skin == "jana"
	or p.mo.skin == "eggman"
	or p.mo.skin == "gemma"
	or p.mo.skin == "kiryu"
	and x.forcehyper <= 0 then
		x.ultrastar = true
		x.hyperimages = true
		x.canhyper = false
	end
	if p.mo.hyperflashcolor == nil
		 p.mo.hyperflashcolor = 1
	 end
	 
	if MRCE_isHyper(p)
		if p.mo.skin == "skip" --SKIP BOOM
		or p.mo.skin == "nasya"
			p.mysticsuper = true
		end
	else
		if p.mo.skin == "skip"
		or p.mo.skin == "nasya"
			p.mysticsuper = false
		end
	end
	if not P_IsObjectOnGround(p.mo)
	and p.mo.skin == "skip" and MRCE_isHyper(p)
		//Hyper float
		if p.cmd.buttons & BT_JUMP
		and p.pflags & PF_JUMPED
		and not (p.pflags & (PF_SPINNING | PF_SHIELDABILITY))
		and not p.climbing and p.secondjump != 255
		and p.mo.momz*P_MobjFlip(p.mo) < 0				
			if p.speed < 5*p.mo.scale
				P_SetObjectMomZ(p.mo, -3*FRACUNIT)
			end
			p.pflags = $ &~ PF_STARTJUMP | PF_NOJUMPDAMAGE
		end
	end
	if p.mo.skin == "shadow" and MRCE_isHyper(p)
		if p.shadow and p.shadow.flags & SHF_SPEARCHARGE
			p.momz = 0
		end
	end
--blendmode secret
	if x.glowaura == 1 then
		p.mo.blendmode = AST_ADD
		p.mo.colorized = true
	elseif x.glowaura == 2 then
		p.mo.blendmode = AST_SUBTRACT
		p.mo.colorized = true
	end

	if p.followmobj and p.mrce.glowaura then
		p.followmobj.blendmode = p.mo.blendmode
		p.followmobj.colorized = p.mo.colorized
	end
	
	if p.pet and p.mo.pet
		p.mo.pet.blendmode = p.mo.blendmode
		if not (p.mo.pet.color == SKINCOLOR_NONE)
			p.mo.pet.colorized = p.mo.colorized
		end
	end

	if p.buddies
		for id,buddy in pairs(p.buddies) do
			if buddy.mo and buddy.mo.valid
				buddy.mo.blendmode = p.mo.blendmode
				buddy.mo.colorized = p.mo.colorized
			end
		end
	end
--hyper related cheat code handling
	if x.hypercheat == true
		if not All7Emeralds(emeralds) and not netgame
			emeralds = 127
		end
		if modifiedgame == false and not netgame
		and not marathonmode and not modeattacking then
			COM_BufInsertText(p, "devmode 1")
			COM_BufInsertText(p, "devmode 0")
		end
	end
	if x.flycheat == true and modifiedgame == false and not netgame
	and not marathonmode and not modeattacking then
		COM_BufInsertText(p, "devmode 1")
		COM_BufInsertText(p, "devmode 0")
	end

--hyper after images
	if ((p.speed >= 8*FRACUNIT or p.mo.momz >= 3*FRACUNIT or p.mo.momz <= -3*FRACUNIT) and x.hyperimages == true) --does our character support hyper afterimages?
	and MRCE_isHyper(p)
	and not p.mo.state == ((p.mo.state == S_PLAY_SUPER_TRANS1) or (p.mo.state == S_PLAY_SUPER_TRANS2) or (p.mo.state == S_PLAY_SUPER_TRANS3) or (p.mo.state == S_PLAY_SUPER_TRANS4) or (p.mo.state == S_PLAY_SUPER_TRANS5) or (p.mo.state == S_PLAY_SUPER_TRANS6))
	and p.playerstate != PST_DEAD then
		local ghostpos = 1
		local AfterImageSpawn = P_SpawnGhostMobj(p.mo)
		AfterImageSpawn.colorized = true
		AfterImageSpawn.fuse = 5
		AfterImageSpawn.destscale = $ * (5/4)
		if p.mrce.glowaura < 2 then
			AfterImageSpawn.blendmode = AST_ADD
		end

		AfterImageSpawn.rollangle = p.mo.rollangle
		
		if p.mo.hyperflashcolor < 5
		or p.mo.hyperflashcolor == 69
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 4
		and p.mo.hyperflashcolor < 7
			AfterImageSpawn.color = SKINCOLOR_SUPERSAPPHIRE1
		end
		
		if p.mo.hyperflashcolor > 6
		and p.mo.hyperflashcolor < 9
		AfterImageSpawn.color = SKINCOLOR_SUPERSAPPHIRE2
		end
		
		if p.mo.hyperflashcolor > 8
		and p.mo.hyperflashcolor < 11
			AfterImageSpawn.color = SKINCOLOR_SUPERSAPPHIRE1
		end
		
		if p.mo.hyperflashcolor > 10
		and p.mo.hyperflashcolor < 13
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 12
		and p.mo.hyperflashcolor < 15
			AfterImageSpawn.color = SKINCOLOR_SUPERBUBBLEGUM1
		end
		
		if p.mo.hyperflashcolor > 14
		and p.mo.hyperflashcolor < 17
			AfterImageSpawn.color = SKINCOLOR_SUPERBUBBLEGUM2
		end
		
		if p.mo.hyperflashcolor > 16
		and p.mo.hyperflashcolor < 19
			AfterImageSpawn.color = SKINCOLOR_SUPERBUBBLEGUM1
		end
		
		if p.mo.hyperflashcolor > 18
		and p.mo.hyperflashcolor < 21
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 20
		and p.mo.hyperflashcolor < 23
			AfterImageSpawn.color = SKINCOLOR_SUPERMINT1
		end
		
		if p.mo.hyperflashcolor > 22
		and p.mo.hyperflashcolor < 25
			AfterImageSpawn.color = SKINCOLOR_SUPERMINT2
		end
		
		if p.mo.hyperflashcolor > 24
		and p.mo.hyperflashcolor < 27
			AfterImageSpawn.color = SKINCOLOR_SUPERMINT1
		end
		
		if p.mo.hyperflashcolor > 26
		and p.mo.hyperflashcolor < 29
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 28
		and p.mo.hyperflashcolor < 31
			AfterImageSpawn.color = SKINCOLOR_SUPERRUBY1
		end
		
		if p.mo.hyperflashcolor > 30
		and p.mo.hyperflashcolor < 33
			AfterImageSpawn.color = SKINCOLOR_SUPERRUBY2
		end
		
		if p.mo.hyperflashcolor > 32
		and p.mo.hyperflashcolor < 35
			AfterImageSpawn.color = SKINCOLOR_SUPERRUBY1
		end
		
		if p.mo.hyperflashcolor > 34
		and p.mo.hyperflashcolor < 37
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 36
		and p.mo.hyperflashcolor < 39
			AfterImageSpawn.color = SKINCOLOR_SUPERWAVE1
		end
		
		if p.mo.hyperflashcolor > 38
		and p.mo.hyperflashcolor < 41
			AfterImageSpawn.color = SKINCOLOR_SUPERWAVE2
		end
		
		if p.mo.hyperflashcolor > 40
		and p.mo.hyperflashcolor < 43
			AfterImageSpawn.color = SKINCOLOR_SUPERWAVE1
		end
		
		if p.mo.hyperflashcolor > 42
		and p.mo.hyperflashcolor < 45
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 44
		and p.mo.hyperflashcolor < 47
			AfterImageSpawn.color = SKINCOLOR_SUPERCOPPER1
		end
		
		if p.mo.hyperflashcolor > 46
		and p.mo.hyperflashcolor < 49
			AfterImageSpawn.color = SKINCOLOR_SUPERCOPPER2
		end
		
		if p.mo.hyperflashcolor > 48
		and p.mo.hyperflashcolor < 51
			AfterImageSpawn.color = SKINCOLOR_SUPERCOPPER1
		end
		
		if p.mo.hyperflashcolor > 50
		and p.mo.hyperflashcolor < 53
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 52
		and p.mo.hyperflashcolor < 55
			AfterImageSpawn.color = SKINCOLOR_SUPERAETHER1
		end
		
		if p.mo.hyperflashcolor > 54
		and p.mo.hyperflashcolor < 57
			AfterImageSpawn.color = SKINCOLOR_SUPERAETHER2
		end
		
		if p.mo.hyperflashcolor > 56
		and p.mo.hyperflashcolor < 59
			AfterImageSpawn.color = SKINCOLOR_SUPERAETHER1
		end
	end
end)

addHook("PlayerCanDamage", function(player, mobj) --hyper forms can pop monitors by just walking into them
	if (MRCE_isHyper(player)) or (player.yusonictable and player.yusonictable.hypersonic and player.mo.skin == "adventuresonic") or (player.hyper and player.hyper.transformed)
		return true
	end
end)

//Literally just a port from the source code aside from the slow fall part
addHook("JumpSpinSpecial", function(player)
	if MRCE_isHyper(player) and player.charability == 18
	and player.speed < 5*player.mo.scale
	and P_MobjFlip(player.mo)*player.mo.momz <= 0
		if player.speed >= FixedMul(player.runspeed, player.mo.scale)
			player.mo.state = S_PLAY_FLOAT_RUN
		else
			player.mo.state = S_PLAY_FLOAT
		end
		P_SetObjectMomZ(player.mo, -3*FRACUNIT)
		player.pflags = $&~(PF_STARTJUMP|PF_SPINNING)
	end
	
	if player.powers[pw_super] and (player.mo.skin == "supersonic" or player.charability == 18)
	and player.speed > 5*player.mo.scale
	and P_MobjFlip(player.mo)*player.mo.momz <= 0
		if player.speed >= FixedMul(player.runspeed, player.mo.scale)
			player.mo.state = S_PLAY_FLOAT_RUN
		else
			player.mo.state = S_PLAY_FLOAT
		end
		P_SetObjectMomZ(player.mo, 0)
		player.pflags = $&~(PF_STARTJUMP|PF_SPINNING)
	end
end)

//Super sparkles


states[S_SUPERSPARK] = {
       sprite = SPR_SUSK,
	   frame = FF_FULLBRIGHT|FF_ANIMATE|A,
	   var1 = 4,
	   var2 = 3
}
mobjinfo[MT_SUPERSPARKLES] = {
         spawnstate = S_SUPERSPARK,
         flags = MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_NOCLIPHEIGHT,
}


addHook("MobjSpawn", function(mobj)
	if netgame then
		mobj.fuse = 30
	end
end, MT_CYBRAKDEMON_FLAMEREST)

addHook("MobjFuse", function(mobj)
	P_RemoveMobj(mobj)
end, MT_CYBRAKDEMON_FLAMEREST)

rawset(_G, "SkipBadnikScrap", {
	[MT_EGGANIMUS_EX] = 100,
})

--Triggers an event if you're SA-Sonic.

local function YuHyperMemories(line, so)
	if so and so.valid and so.player and so.player.yusonictable and not so.player.yusonictable.hyperpower and so.skin == "adventuresonic" and so.health
		S_StartSound(so, sfx_s3k7d)
		S_StartSound(so, sfx_s3kcel)
		so.player.yusonictable.hypermemories = 220
		so.player.yusonictable.superflash = 9*FRACUNIT
		so.player.yusonictable.hyperpower = true
		return false
	end
end
addHook("LinedefExecute", YuHyperMemories, "YUMEMO")

local bossesInfo = {
	{MT_EGGANIMUS, "Egg Animus", "MASTER OF LIGHT"},
	{MT_EGGANIMUS_EX, "Egg Animus EX", "MASTER OF LIGHT"},
	{MT_EGGBALLER, "Egg FireBaller", "BLAZING N BALLIN"},
	{MT_EGGEBOMBER, "Egg E-Bomber", "EXPLOSIVE SHOCKER"},
	{MT_EGGFREEZER, "Egg Freezer", "FROZEN NIGHTMARE"},
	{MT_FBOSS, "Egg Fighter", "SPARKING DEFENDER"},
	{MT_FBOSS2, "Egg Fighter", "SPARKING DEFENDER"},
	{MT_XBOSS, "Egg Mobile X", "ARMED SUPER WEAPON"}
}
if yakuzaBossTexts then
	for _,val in ipairs(bossesInfo) do
		if not (val[3]) then
			val[3] = ""
		end
		yakuzaBossTexts[val[1]] = {name = val[2], info = val[3]}
	end
end

if not adventureBossTable then
    rawset(_G, "adventureBossTable", {})
end

adventureBossTable[MT_EGGANIMUS] = {"Egg Animus", 75, 1000}
adventureBossTable[MT_EGGANIMUS_EX] = {"Egg Animus EX", 100, 2500}
adventureBossTable[MT_EGGBALLER] = {"Egg FireBaller", 25, 100}
adventureBossTable[MT_EGGEBOMBER] = {"Egg E-Bomber", 45, 250}
adventureBossTable[MT_EGGFREEZER] = {"Egg Freezer", 35, 350}
adventureBossTable[MT_FBOSS] = {"Egg Fighter", 45, 500}
adventureBossTable[MT_FBOSS2] = {"Egg Fighter", 55, 750}
adventureBossTable[MT_XBOSS] = {"Egg Mobile X", 35, 750}

addHook("MobjSpawn", function(speccy)
	speccy.scale = FRACUNIT/2
	speccy.color = SKINCOLOR_CARBON
end, MT_SPECCY)

addHook("LinedefExecute", function(line, mo)
	print("talking fox")
	if not (mo.player) then
		print("bad mo")
		return
	end
	local speccytalk = {
		active = true,
		inputblock = true;
		[1] = {
			charpic = "STARORB",
			line = "what."
		}
/*		[2] = {
			charpic = "STARORB",
			line = "Here is the second page for this dialog box. I didn't care about making sure words didn't get cut up on this one. but hey. It's still cool"
		},
		[3] = {
			charpic = "STARORB",
			line = "Did you know that pressing spin at the end of the dialog box will go back a page? Try it at the end of this page!"
		}*/
	}
	event_CallDialog(speccytalk)
end,"SPECCYT")

addHook("NetVars", function(net)
	mrce_dowarptime = net($)
	mrce_hyperstones = net($)
	mrce_hyperunlocked = net($)
	mrce_secondquest = net($)
end)
