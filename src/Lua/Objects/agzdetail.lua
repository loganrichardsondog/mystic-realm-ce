/*
	AGZ Detailing objects
	
	(C) 2022 by K. "ashifolfi" J.
*/

freeslot(
	"MT_TREEBRANCH",
	"MT_TREEBUSH",
	
	// Shared states. sprite changing is done via object specific logic because I can't be bothered.
	"S_AGZDETAIL_NOBOARD",
	"S_AGZDETAIL_REGULAR"
)

// Shared state definitions
states[S_AGZDETAIL_NOBOARD] = {SPR_UNKN, A|FF_PAPERSPRITE, -1, A_NULL, 0, 0, S_NULL}
states[S_AGZDETAIL_REGULAR] = {SPR_UNKN, A, -1, A_NULL, 0, 0, S_NULL}

mobjinfo[MT_TREEBRANCH] = {
	doomednum = 1341,
	spawnstate = S_AGZDETAIL_REGULAR,
	flags = MF_SCENERY
}

mobjinfo[MT_TREEBUSH] = {
	--$Name AGZ Tree Bush
	--$Category AGZ Details
	--$Sprite KHB1A0
	doomednum = 1340,
	spawnstate = S_AGZDETAIL_NOBOARD,
	seestate = S_AGZDETAIL_NOBOARD,
	spawnhealth = -1,
	flags = MF_NOGRAVITY|MF_NOBLOCKMAP
}

// Bush code
addHook("MobjThinker", function(mo)
	mo.sprite = SPR_KHB1
	mo.frame = A
	mo.health = 50000
end, MT_TREEBUSH)