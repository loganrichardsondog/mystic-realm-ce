freeslot(
"SPR_UNCEMB",
"SPR_KEY0L",
"SPR_KEY1S",
"SPR_KEY2S",
"SPR_KEY3S",
"MT_FIRE_STONE",
"MT_WATER_STONE",
"MT_LIGHTNING_STONE"
)
local emblemlist = {}
local offset = 320
local offset2 = 320
local offsetresettimer = 0
local offsetresettimer2 = 0
local tabresettimer = 2
local touchedresettimer = 25
local tabMenuOpened = nil

addHook("PlayerSpawn", function()
	emblemlist = {}
	for mapemblem in mobjs.iterate() do
		if mapemblem.valid and mapemblem.type == MT_EMBLEM then
			local emblem = {frame = mapemblem.frame, color = mapemblem.color, origmobj = mapemblem}
			table.insert(emblemlist, emblem)
		end
	end
end)

addHook("TouchSpecial", function(emblem, pmo)
	pmo.player.changeoffset = 1
	offsetresettimer = touchedresettimer
end, MT_EMBLEM)

addHook("TouchSpecial", function(key, pmo)
	pmo.player.changeoffset2 = 1
	offsetresettimer2 = touchedresettimer
end, MT_LIGHTNING_STONE)

addHook("TouchSpecial", function(key, pmo)
	pmo.player.changeoffset2 = 1
	offsetresettimer2 = touchedresettimer
end, MT_FIRE_STONE)

addHook("TouchSpecial", function(key, pmo)
	pmo.player.changeoffset2 = 1
	offsetresettimer2 = touchedresettimer
end, MT_WATER_STONE)

addHook("ThinkFrame", function()
	for p in players.iterate do
		if not p.bot and p.valid then
			if tabMenuOpened == true then
				p.changeoffset = 1
				p.changeoffset2 = 1
				offsetresettimer = 2
				tabMenuOpened = false
			end
			if p.changeoffset then
				if offset > 160 then
					offset = $ - 10
				end
				if offset == 160 and offsetresettimer then
					offsetresettimer = $ - 1
				end
				if not offsetresettimer then
					p.changeoffset = 0
				end
			else
				if offset < 320 then
					offset = $ + 20
				end
			end
			if p.changeoffset2 then
				if offset2 > 260 then
					offset2 = $ - 10
				end
				if offset2 == 260 and offsetresettimer2 then
					offsetresettimer2 = $ - 1
				end
				if not offsetresettimer2 then
					p.changeoffset2 = 0
				end
			else
				if offset2 < 320
					offset2 = $ + 20
				end
			end
		end
	end
end)

hud.add(function(d, p)
	if netgame then return end
	local loffset = offset
	local soffset = offset2
	local bg = d.cachePatch("EMBBG")
	d.draw(loffset, 19, bg, V_SNAPTORIGHT|V_SNAPTOTOP)
	d.draw(soffset, 48, bg, V_SNAPTORIGHT|V_SNAPTOTOP)
	local uncollectedkey = d.cachePatch("KEY0L")
	local key1 = d.getSpritePatch("KYST", A)
	local key2 = d.getSpritePatch("KYST", B)
	local key3 = d.getSpritePatch("KYST", C)
	if not (mrce_hyperstones & (1 << (0))) then
		d.drawScaled((soffset*FRACUNIT + ((uncollectedkey.width / 4)*FRACUNIT) + 8*FRACUNIT), 51*FRACUNIT, FRACUNIT/2, uncollectedkey, V_SNAPTORIGHT|V_SNAPTOTOP, emblemcolor)
	else
		d.drawScaled((soffset*FRACUNIT + ((key1.width / 4)*FRACUNIT) + 8*FRACUNIT), 51*FRACUNIT, FRACUNIT/2, key1, V_SNAPTORIGHT|V_SNAPTOTOP, emblemcolor)
	end
	if not (mrce_hyperstones & (1 << (1))) then
		d.drawScaled((soffset*FRACUNIT + ((uncollectedkey.width / 4)*FRACUNIT) + 24*FRACUNIT), 51*FRACUNIT, FRACUNIT/2, uncollectedkey, V_SNAPTORIGHT|V_SNAPTOTOP, emblemcolor)
	else
		d.drawScaled((soffset*FRACUNIT + ((key2.width / 4)*FRACUNIT) + 24*FRACUNIT), 51*FRACUNIT, FRACUNIT/2, key2, V_SNAPTORIGHT|V_SNAPTOTOP, emblemcolor)
	end
	if not (mrce_hyperstones & (1 << (2))) then
		d.drawScaled((soffset*FRACUNIT + ((uncollectedkey.width / 4)*FRACUNIT) + 40*FRACUNIT), 51*FRACUNIT, FRACUNIT/2, uncollectedkey, V_SNAPTORIGHT|V_SNAPTOTOP, emblemcolor)
	else
		d.drawScaled((soffset*FRACUNIT + ((key3.width / 4)*FRACUNIT) + 40*FRACUNIT), 51*FRACUNIT, FRACUNIT/2, key3, V_SNAPTORIGHT|V_SNAPTOTOP, emblemcolor)
	end
	for embnum, emblem in ipairs(emblemlist) do
		local emblempatch = d.getSpritePatch("EMBM", emblem.frame & FF_FRAMEMASK)
		local uncollectedemb = d.cachePatch("UNCEMB")
		local emblemcolor = d.getColormap(TC_DEFAULT, emblem.color)
		d.drawScaled((loffset*FRACUNIT + ((uncollectedemb.width / 4)*FRACUNIT)), 24*FRACUNIT, FRACUNIT/2, uncollectedemb, V_SNAPTORIGHT|V_SNAPTOTOP, emblemcolor)
		if (emblem.frame & FF_TRANSMASK) or (not emblem.origmobj.valid) then
			d.drawScaled((loffset*FRACUNIT + ((emblempatch.width / 4)*FRACUNIT)), 24*FRACUNIT, FRACUNIT/2, emblempatch, V_SNAPTORIGHT|V_SNAPTOTOP, emblemcolor)
		end
		loffset = $ + 32
		soffset = $ + 32
	end
end, "game")

hud.add(function(d, p)
	tabMenuOpened = true
	offsetresettimer = tabresettimer
	offsetresettimer2 = tabresettimer
	if netgame then
		local key1 = d.cachePatch("KEY1S")
		local key2 = d.cachePatch("KEY2S")
		local key3 = d.cachePatch("KEY3S")
		if (mrce_hyperstones & (1 << (0))) then
			d.draw(20, 13, key1, V_SNAPTOLEFT|V_SNAPTOTOP)
		end
		if (mrce_hyperstones & (1 << (1))) then
			d.draw(30, 13, key2, V_SNAPTOLEFT|V_SNAPTOTOP)
		end
		if (mrce_hyperstones & (1 << (2))) then
			d.draw(40, 13, key3, V_SNAPTOLEFT|V_SNAPTOTOP)
		end
	end
end, "scores")
