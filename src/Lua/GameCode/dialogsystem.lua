/*
	Dialog system

	(C) 2022 by Ashi
*/
-- logging purposes
print("Initializing Dialog System...")

-- variables (derogatory)
local eventinfo = {}
local page = 1
local textprogress = {}
local MAXLINE = 32
local WAITTIME = 132

-- testing command
/*
COM_AddCommand("test_dialog", function(player)
	-- testing dialog table
	-- use this as an example of how to make a dialog event!
	local testevent = {
		active = true,
		inputblock = true;
		[1] = {
			charpic = "STARORB",
			line = "Testing the dialog system out. We have full control over every aspect of the dialog with this. It even autowraps the lines for you!"
		},
		[2] = {
			charpic = "STARORB",
			line = "Here is the second page for this dialog box. I didn't care about making sure words didn't get cut up on this one. but hey. It's still cool"
		},
		[3] = {
			charpic = "STARORB",
			line = "Did you know that pressing spin at the end of the dialog box will go back a page? Try it at the end of this page!"
		}
	}
	event_CallDialog(testevent)
end)
*/
local prev_page

-- event_CallDialog
-- Call open the dialog box for your event
-- Sets everything up for a new event
---@param event table Event information
local function event_CallDialog(event)
	if (type(event) != "table") then
		error("CALL_FAILED: no valid event table")
		return
	end
	if (eventinfo.current and eventinfo.current.active) then
		error("CALL_FAILED: event currently active")
		return
	end
	-- Reset page variables on a new event
	prev_page = nil
	page = 1
	eventinfo.current = event
end

local function hud_dialogdraw(v, p)
	if (eventinfo.current == nil) then return end
	if (eventinfo.current.active == false) then return end

	-- reset everything on a new page.
	if (prev_page != page) then
		prev_page = page
		-- make sure every text progress is 1
		for ln=1,5 do
			textprogress[ln] = 0
		end
	end
	
	-- draw the dialog box
	v.drawFill(0, 150, 320, 50, 31)

	-- draw the character picture
	v.draw(0, 150, v.cachePatch(eventinfo.current[page].charpic))

	-- line wrapping(TM)(R)
	eventinfo.current[page].sep_line = {}
	/*funny shortcut*/ local sep_line = eventinfo.current[page].sep_line

	for i=1,5 do
		sep_line[i] = string.sub(eventinfo.current[page].line, MAXLINE*(i-1), (MAXLINE*i)-1/*prevent duped characters :P*/)
	end

	-- Variable for the text height
	local y = 150
	for ln=1,5 do
		-- self explanatory
		v.drawString(40, y, string.sub(sep_line[ln], 0, textprogress[ln]))

		if (ln > 1) then
			-- wait your turn
			if (textprogress[ln-1] < #sep_line[ln-1]) then
				return
			end
			-- I control the speed at which the text draws!
			if (leveltime % 2 == 0)
			or (eventinfo.current.speedup == true) then
				textprogress[ln] = $ + 1
			end
		else
			-- :P
			if (leveltime % 2 == 0)
			or (eventinfo.current.speedup == true) then
				textprogress[ln] = $ + 1
			end
		end

		y = $ + 10
	end

	if not(eventinfo.current.inputblock) then
		if (textprogress[#sep_line] >= #sep_line[#sep_line]) then -- are we finished displaying text?
			if (page == #eventinfo.current) then
				eventinfo.current = nil
				return
			end
			if (WAITTIME > 0) then
				WAITTIME = $ - 1
			else
				WAITTIME = 32
				page = $ + 1
			end
		end
	end

	-- we are at the end of the dialog. dismiss and clean up
	--eventinfo.current = nil
	--page = 0
	--textprogress = 0
end

-- input handler for dialog events
addHook("KeyDown", function(key)
	-- do we even have an event?
	if (eventinfo.current == nil) then return end
	if (eventinfo.current.active == false) then return end

	/*funny shortcut*/ local sep_line = eventinfo.current[page].sep_line

	-- do we brick wall the controller?
	if (eventinfo.current.inputblock == true) then
		for i=1,2 do
			if (key.num == ctrl_inputs.jmp[i]) then -- is the player pressing jump?
				if (textprogress[#sep_line] >= #sep_line[#sep_line]) then -- are we finished displaying text?
					if (page == #eventinfo.current) then -- Is this the end of the event?
						eventinfo.current = nil -- yes, dismiss the event
						return true
					else
						page = $ + 1 -- no, progress a page
					end
					return true
				end
				return true
			elseif (key.num == ctrl_inputs.spn[i]) then -- is the player pressing spin?
				if not(sep_line) then return true end
				if (textprogress[#sep_line] >= #sep_line[#sep_line]) -- If we are at the end of a dialog box
				and (eventinfo.current.speedup == false) then -- don't want to accidentally trigger it
					if (page != 1) then -- and we are not on page 1
						page = $ - 1 -- go back a page
						return true
					end
					return true
				else
					eventinfo.current.speedup = true
					return true
				end
				return true
			end

			-- *holding sonic* >:P no move. only dialog.
			if (key.num == ctrl_inputs.up[i] or key.num == ctrl_inputs.down[i])
			or (key.num == ctrl_inputs.left[i] or key.num == ctrl_inputs.right[i])
			-- no custom ability either. only dialog. >:P
			or (key.num == ctrl_inputs.cb1[i] or key.num == ctrl_inputs.cb2[i])
			or (key.num == ctrl_inputs.cb3[i]) then
				return true
			end
		end
	end
end)

addHook("KeyUp", function(key)
	-- do we even have an event?
	if (eventinfo.current == nil) then return end
	if (eventinfo.current.active == false) then return end

	if (eventinfo.current.inputblock == true) then
		for i=1,2 do
			if (key.num == ctrl_inputs.spn[i]) then -- Is the player releasing spin?
				eventinfo.current.speedup = false -- set speedup to false
			end
		end
	end
end)

-- create global functions
rawset(_G, "event_CallDialog", event_CallDialog)

-- add hud elements
hud.add(hud_dialogdraw, "game")

-- Print out a success message
print("Dialog System Initialized")