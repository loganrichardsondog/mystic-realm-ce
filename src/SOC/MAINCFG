#------------------------------------------------------------------------------------
# Configuration File for Mystic Realm
#------------------------------------------------------------------------------------
# Mystic Realm: Community Edition, a level pack for Sonic Robo Blast 2 Version 2.2
#------------------------------------------------------------------------------------
# This is the Mystic Realm SOC, or SRB2 Object Configuration. Originally all the data
#  was stored here in MAINCFG, but now SRB2 supports splitting the SOC into multiple
#  lumps for organization.
# All text after the hash is a comment, and is ignored by SRB2 when it reads the
#  file.
#------------------------------------------------------------------------------------

# This line tells the game what version this mod is intended for.

SRB2 220

# MainCFG section, sets various global variables and what files for saved data.

Maincfg Category
IntroToPlay = 1
NumDemos = 4
DemoDelayTime = 60*TICRATE
TitlePicsMode = NONE
LoopTitle = true
TitleScrollSpeed = 20
TitlePicsScalesAvailable = 4,5
TitleMap = T0
TutorialMap = 0
CustomVersion = CE 0.3
SPStage_Start = 99
SPMarathon_Start = 101
UseBlackRock = true
Gamedata = sl_mrce-beta.dat

# Clear removes any vanilla configuration information

Clear All

# Unlockables explanation: 
#  https://wiki.srb2.org/wiki/Custom_unlockables_and_emblems

ConditionSet 1
Condition1 = MapBeaten 101

ConditionSet 2
Condition1 = GameClear

ConditionSet 3
Condition1 = AllEmeralds

ConditionSet 4
Condition1 = MapBeaten 164

ConditionSet 5
Condition1 = TotalEmblems 30

ConditionSet 6
Condition1 = TotalEmblems 60

ConditionSet 7
Condition1 = TotalEmblems 150

ConditionSet 8
Condition1 = MapBeaten 123

ConditionSet 9
Condition1 = MapBeaten 124

ConditionSet 10
Condition1 = MapBeaten 125

ConditionSet 11
Condition1 = MapBeaten 126

ConditionSet 12
Condition1 = MapBeaten 127

ConditionSet 13
Condition1 = MapBeaten 128

ConditionSet 14
Condition1 = MapBeaten 129

ConditionSet 15
Condition1 = MapBeaten 130

ConditionSet 16
Condition1 = MapBeaten 131

ConditionSet 17
Condition1 = TotalEmblems 90

Unlockable 1
Name = Record Attack
Objective = Complete Jade Coast Zone Act 1
Type = RecordAttack
NoCecho = true
NoChecklist = true
ConditionSet = 1

Unlockable 2
Name = Fang
Objective = Complete 1P Mode
Type = None
ConditionSet = -1

Unlockable 3
Name = Metal
Objective = Complete 1P Mode
Type = None
ConditionSet = -1

Unlockable 23
Name = Amy
Objective = Complete 1P Mode
Type = None
ConditionSet = -1

Unlockable 11
Name = Emerald Stages
Objective = Complete an Emerald Stage
NoCecho = false
NoChecklist = false
Type = LevelSelect
ConditionSet = -1
Var = 1

Unlockable 12
Name = Sound Test
Objective = Complete the game with all emeralds
Type = SoundTest
Height = 20
ConditionSet = 3
Var = 2

Unlockable 13
Name = Bonus Stages
Objective = Clear Mystic Realm Zone
Height = 90
ConditionSet = 3
Type = LevelSelect
Var = 2

Unlockable 14
Name = Prismatic Angel Zone
Objective = Clear Mystic Realm Zone
Height = 105
ConditionSet = 3
NoCecho = true
NoChecklist = true
Type = Warp
Var = 132

Unlockable 15
Name = Mystic Realm Zone
Objective = Clear Mystic Realm Zone
Height = 115
ConditionSet = 3
NoCecho = true
NoChecklist = true
Type = Warp
Var = 135

Unlockable 16
Name = Emblem Hints
Objective = Get 30 emblems
Height = 0
ConditionSet = -1
Type = EmblemHints

Unlockable 17
Name = Emblem Radar
Objective = Get 60 emblems
Height = 0
ConditionSet = 6
Type = ItemFinder

Unlockable 18
Name = Making of Mystic Realm
Objective = Get 90 emblems
Height = 60
ConditionSet = 17
Type = Warp
Var = 100

Unlockable 19
Name = Pandora's Box
Objective = Get 150 emblems
Type = Pandora
ConditionSet = 7

Unlockable 20
Name = Play Credits
Objective = Complete the game
Height = 40
ConditionSet = 2
Type = Warp
Var = 98
NoCecho = true

Unlockable 21
Name = Second Quest
Objective = Beat the game With All Emeralds
Height = 50
ConditionSet = 4
NoChecklist = true
Type = None

# Emblem Data. 
#  https://wiki.srb2.org/wiki/Custom_unlockables_and_emblems

ExtraEmblem 1
Name = Game Complete
Objective = Complete 1P Mode
ConditionSet = 2
SPRITE = F
COLOR = SKINCOLOR_LAVENDER

ExtraEmblem 2
Name = All Emerald Clear
Objective = Complete 1P Mode w/ All Emeralds
ConditionSet = 3
SPRITE = H
COLOR = SKINCOLOR_GREEN

Emblem
Type = Time
Var = 35*TICRATE
MapNum = 101
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 400
MapNum = 101
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = This one is easy.\\It's in a circle of rings.\\Just run straight ahead.
Type = Global
Tag = 0
MapNum = 101
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = High above the goal.\\Fly or climb up to the cave.\\The prize lies inside.
Type = Global
Tag = 1
MapNum = 101
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = It's a common trope:\\A secret hidden behind\\the lone waterfall.
Type = Global
Tag = 2
MapNum = 101
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Hint = A bridge tunnel lies above.\\You ever wondered what lies\\on top?.
Type = Global
Tag = 3
MapNum = 101
Sprite = D
Color = SKINCOLOR_PURPLE

Emblem
Hint = A small lake.\\Don't take a dip.\\Look up and you shall see.
Type = Global
Tag = 4
MapNum = 101
Sprite = E
Color = SKINCOLOR_ORANGE

Emblem
Type = Time
Var = 50*TICRATE
MapNum = 102
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 400
MapNum = 102
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = You cannot progress\\without going in a cave.\\Try searching high first.
Type = Global
Tag = 0
MapNum = 102
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = A giant waterfall from\\above. Follow it down into\\a small cave.
Type = Global
Tag = 1
MapNum = 102
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = I climbed so far up,\\I can see my house from here!\\...Well, it's someone's house.
Type = Global
Tag = 2
MapNum = 102
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Hint = Inside the coral cave.\\Spindash across the\\water into springs.
Type = Global
Tag = 3
MapNum = 102
Sprite = D
Color = SKINCOLOR_PURPLE

Emblem
Hint = Two paths become one.\\turn back to find another.\\Follow to the shrine.
Type = Global
Tag = 4
MapNum = 102
Sprite = I
Color = SKINCOLOR_BLACK

Emblem
Type = Score
Var = 10000
MapNum = 103
Sprite = S
Color = SKINCOLOR_BROWN

Emblem
Type = Time
Var = 30*TICRATE
MapNum = 103
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Time
Var = 120*TICRATE
MapNum = 104
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 500
MapNum = 104
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = Jump on the platform,\\turn left, then go through the goop.\\It's right at the start.
Type = Global
Tag = 0
MapNum = 104
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = After a small tunnel,\\look up for a small cliff\\Some awaits at the top.
Type = Global
Tag = 1
MapNum = 104
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = A twin goop falls.\\Where does it come from?\\Only one wait to find out?
Type = Global
Tag = 2
MapNum = 104
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Hint = Inside a cave.\\Tons of falling goop.\\One of them seems different.
Type = Global
Tag = 3
MapNum = 104
Sprite = D
Color = SKINCOLOR_PURPLE

Emblem
Hint = Find a tunnel in\\the room full of moving goop.\\Jump when it's lowest.
Type = Global
Tag = 4
MapNum = 104
Sprite = I
Color = SKINCOLOR_BLACK

Emblem
Type = Time
Var = 40*TICRATE
MapNum = 105
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 250
MapNum = 105
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = Jump on the platform,\\turn left, then go through the goop.\\It's right at the start.
Type = Global
Tag = 0
MapNum = 105
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = Wouldn't it be fun\\if you could travel up a\\goopy waterfall?
Type = Global
Tag = 1
MapNum = 105
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = Room of moving goop.\\Wait until the goop is low,\\then jump down in it.
Type = Global
Tag = 2
MapNum = 105
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Type = Score
Var = 10000
MapNum = 106
Sprite = S
Color = SKINCOLOR_BROWN

Emblem
Type = Time
Var = 30*TICRATE
MapNum = 106
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Time
Var = 40*TICRATE
MapNum = 107
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 250
MapNum = 107
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = Inside a big tree.\\Normally you would go down.\\But instead, go up.
Type = Global
Tag = 0
MapNum = 107
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = It's between two trees,\\in the deep part of a lake.\\Near a whirlwind shield.
Type = Global
Tag = 1
MapNum = 107
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = Up above a lake,\\there is a hole in the trees.\\Fly or use a spring.
Type = Global
Tag = 2
MapNum = 107
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Type = Time
Var = 50*TICRATE
MapNum = 108
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 200
MapNum = 108
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = On the upper route,\\go inside the giant tree,\\and then turn around.
Type = Global
Tag = 0
MapNum = 108
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = On the lower path,\\hanging precariously\\out on a tree branch.
Type = Global
Tag = 1
MapNum = 108
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = In the canopy,\\check behind some of the trees.\\This emblem blends in.
Type = Global
Tag = 2
MapNum = 108
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Hint = After jumping down,\\check the tree in the corner.\\Go inside the hole.
Type = Global
Tag = 3
MapNum = 108
Sprite = I
Color = SKINCOLOR_BLACK

Emblem
Type = Score
Var = 10000
MapNum = 109
Sprite = S
Color = SKINCOLOR_BROWN

Emblem
Type = Time
Var = 20*TICRATE
MapNum = 109
Sprite = T
Color = SKINCOLOR_GREY


Emblem
Type = Time
Var = 105*TICRATE
MapNum = 110
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 400
MapNum = 110
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = Soon after the start,\\near the top of a big machine,\\right behind a torch.
Type = Global
Tag = 0
MapNum = 110
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = The crawlas are digging everywhere!\\What are they looking for?\\try checking their holes.
Type = Global
Tag = 1
MapNum = 110
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = Red spring to great heights,\\then turn around and jump through\\the hole in the wall.
Type = Global
Tag = 2
MapNum = 110
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Hint = A locked metal door,\\3 yellow buttons might open it,\\but where are they?
Type = Global
Tag = 3
MapNum = 110
Sprite = D
Color = SKINCOLOR_PURPLE

Emblem
Hint = Check around the cliffs\\before you go through the caves.\\A secret tunnel.
Type = Global
Tag = 4
MapNum = 110
Sprite = H
Color = Check around the cliffs
Color = SKINCOLOR_YELLOW


Emblem
Type = Time
Var = 105*TICRATE
MapNum = 111
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 400
MapNum = 111
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Type = Global
Tag = 0
MapNum = 111
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Type = Global
Tag = 1
MapNum = 111
Sprite = B
Color = SKINCOLOR_RED

Emblem
Type = Global
Tag = 2
MapNum = 111
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Type = Global
Tag = 3
MapNum = 111
Sprite = D
Color = SKINCOLOR_PURPLE

Emblem
Type = Global
Tag = 4
MapNum = 111
Sprite = I
Color = SKINCOLOR_BLACK

Emblem
Type = Score
Var = 10000
MapNum = 112
Sprite = S
Color = SKINCOLOR_BROWN

Emblem
Type = Time
Var = 30*TICRATE
MapNum = 112
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Time
Var = 20*TICRATE
MapNum = 113
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 200
MapNum = 113
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = The water is cold,\\but not too cold for a swim.\\Check in the tunnel.
Type = Global
Tag = 0
MapNum = 113
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = Go take a detour\\on the slippery ice bridge.\\It leads to a cliff.
Type = Global
Tag = 1
MapNum = 113
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = Look for a cracked wall\\after treading carefully\\across the ice bridge.
Type = Global
Tag = 2
MapNum = 113
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Type = Time
Var = 30*TICRATE
MapNum = 114
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 200
MapNum = 114
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = A flowing river\\covered by a sheet of ice.\\Secrets underneath.
Type = Global
Tag = 0
MapNum = 114
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = A dangerous jump\\in an underwater path.\\A hole in the ice.
Type = Global
Tag = 1
MapNum = 114
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = A hole in the ice\\right before the mystic shrine.\\There's good stuff down there.
Type = Global
Tag = 2
MapNum = 114
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Hint = There's snow on the ice\\just as soon as you can see\\Eggman's ugly face.
Type = Global
Tag = 3
MapNum = 114
Sprite = I
Color = SKINCOLOR_BLACK

Emblem
Type = Score
Var = 10000
MapNum = 115
Sprite = S
Color = SKINCOLOR_BROWN

Emblem
Type = Time
Var = 30*TICRATE
MapNum = 115
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Time
Var = 80*TICRATE
MapNum = 116
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 300
MapNum = 116
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = The most sunken part.\\A deep, little island in\\the ruined left path.
Type = Global
Tag = 0
MapNum = 116
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = Near the first post, check\\out the rightmost waterfall.\\Where does it come from?
Type = Global
Tag = 1
MapNum = 116
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = The last red button\\will unlock a door that is\\near the last starpost.
Type = Global
Tag = 2
MapNum = 116
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Type = Time
Var = 50*TICRATE
MapNum = 117
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 250
MapNum = 117
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = On the lower path,\\right before the last big room,\\look behind the crates.
Type = Global
Tag = 0
MapNum = 117
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = On the upper path,\\right before the last big room,\\look behind the crates.
Type = Global
Tag = 1
MapNum = 117
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = Buttons unlock doors;\\the last one opens a door\\to one more button.
Type = Global
Tag = 2
MapNum = 117
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Hint = See the grey boxes\\just before the final jump?\\Go around the side.
Type = Global
Tag = 3
MapNum = 117
Sprite = I
Color = SKINCOLOR_BLACK

Emblem
Type = Score
Var = 10000
MapNum = 118
Sprite = S
Color = SKINCOLOR_BROWN

Emblem
Type = Time
Var = 30*TICRATE
MapNum = 118
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Time
Var = 20*TICRATE
MapNum = 119
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 150
MapNum = 119
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = How high can you go\\in the very first room of\\Aerial Garden?
Type = Global
Tag = 0
MapNum = 119
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = There are only two\\ways to go in this room, and\\one is the exit.
Type = Global
Tag = 1
MapNum = 119
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = Use your camera\\to look off the edge behind\\the smaller fountain.
Type = Global
Tag = 2
MapNum = 119
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Hint = Triangle buttons\\on both the left and right paths\\will open a door.
Type = Global
Tag = 3
MapNum = 120
Sprite = I
Color = SKINCOLOR_BLACK

Emblem
Type = Time
Var = 70*TICRATE
MapNum = 120
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 300
MapNum = 120
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = A chandelier hangs\\from the ceiling of a room\\where two paths converge.
Type = Global
Tag = 0
MapNum = 120
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = The second left path's\\top entrance is blocked with bars.\\Try going around.
Type = Global
Tag = 1
MapNum = 120
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = Beginning fountain,\\second left and right paths, and\\the final temple.
Type = Global
Tag = 2
MapNum = 120
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Type = Time
Var = 70*TICRATE
MapNum = 121
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Hint = There is a way to\\get outside of the main room.\\Look for a small hole.
Type = Global
Tag = 0
MapNum = 121
Sprite = G
Color = SKINCOLOR_YELLOW

Emblem
Type = Score
Var = 10000
MapNum = 122
Sprite = S
Color = SKINCOLOR_BROWN

Emblem
Type = Time
Var = 30*TICRATE
MapNum = 122
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Time
Var = 45*TICRATE
MapNum = 123
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 225
MapNum = 123
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = When you cross a bridge,\\jump to a platform below.\\There lies your emblem.
Type = Global
Tag = 0
MapNum = 123
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = Three ring monitors,\\on a rock in a cavern.\\You'll need to climb up.
Type = Global
Tag = 1
MapNum = 123
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = Flying off a ramp.\\Before the checkpoint, look back.\\Jump and break some spikes.
Type = Global
Tag = 2
MapNum = 123
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Hint = Bounce from a plateau\\and go flying through the sky\\to reach the platform.
Type = Global
Tag = 3
MapNum = 123
Sprite = D
Color = SKINCOLOR_PURPLE

Emblem
Hint = When life gives you a\\big old ramp to slide off of,\\get mad! Rebound back!
Type = Global
Tag = 4
MapNum = 123
Sprite = E
Color = SKINCOLOR_ORANGE

Emblem
Type = Time
Var = 40*TICRATE
MapNum = 124
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 150
MapNum = 124
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = Go out the side door\\and climb over the big wall.\\Flying will work too.
Type = Global
Tag = 0
MapNum = 124
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = At the first starpost,\\turn to your right. You will see\\a crushing gauntlet.
Type = Global
Tag = 1
MapNum = 124
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = At the second post,\\turn to your right. You will see\\a puddle of goop.
Type = Global
Tag = 2
MapNum = 124
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Type = Time
Var = 60*TICRATE
MapNum = 125
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 50
MapNum = 125
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = Well, so far so good.\\You've made it to the star post.\\Why not look around?
Type = Global
Tag = 0
MapNum = 125
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = Emerald in sight...\\But don't be in such a rush.\\Take a look around.
Type = Global
Tag = 1
MapNum = 125
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = Alright! You did it!\\You made it through the forest!\\But go back for this...
Type = Global
Tag = 2
MapNum = 125
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Type = Time
Var = 70*TICRATE
MapNum = 126
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 200
MapNum = 126
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = Just beyond crushers,\\and underneath an orange pipe.\\Not far from the start.
Type = Global
Tag = 0
MapNum = 126
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = Go where you can find\\Attraction in the corner.\\Listen for some steam.
Type = Global
Tag = 1
MapNum = 126
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = Emerald in sight!\\Lava flows from the ceiling,\\look behind the falls.
Type = Global
Tag = 2
MapNum = 126
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Type = Time
Var = 60*TICRATE
MapNum = 127
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 150
MapNum = 127
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = Taking the left path,\\look behind a pillar at\\the second star post.
Type = Global
Tag = 0
MapNum = 127
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = Taking the right path,\\look behind a pillar at\\the second star post.
Type = Global
Tag = 1
MapNum = 127
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = Look along the walls\\in the last big room that's filled\\with pillars and springs.
Type = Global
Tag = 2
MapNum = 127
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Type = Time
Var = 70*TICRATE
MapNum = 128
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 200
MapNum = 128
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = Sitting on top of\\the first turret you see when\\you go to the left.
Type = Global
Tag = 0
MapNum = 128
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = Double trouble bridge.\\Jump down to get to safety,\\and grab some goodies.
Type = Global
Tag = 1
MapNum = 128
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = Acid flowing out\\of the building near the end.\\Better bring a shield.
Type = Global
Tag = 2
MapNum = 128
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Hint = Find a loose statue,\\push it down a dried up stream.\\A secret button!
Type = Global
Tag = 3
MapNum = 129
Sprite = I
Color = SKINCOLOR_BLACK

Emblem
Type = Time
Var = 70*TICRATE
MapNum = 129
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 200
MapNum = 129
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = Water flowing out\\of a really big fountain.\\This one is easy.
Type = Global
Tag = 0
MapNum = 129
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = Look for this one by\\climbing up a waterfall.\\The one on the left.
Type = Global
Tag = 1
MapNum = 129
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = An elevator\\ride would be way too easy.\\Take the long way up.
Type = Global
Tag = 2
MapNum = 129
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Type = Score
Var = 100000
MapNum = 130
Sprite = S
Color = SKINCOLOR_BROWN

Emblem
Hint = How did this get here?\\Knuckles is gonna be pissed.\\Better not break it.
Type = Global
Tag = 0
MapNum = 130
Sprite = G
Color = SKINCOLOR_YELLOW

Emblem
Type = Score
Var = 200000
MapNum = 131
Sprite = S
Color = SKINCOLOR_BROWN

Emblem
Hint = Welcome to Warp Zone!\\...Wait, that's from some other game.\\Enjoy some free stuff!
Type = Global
Tag = 0
MapNum = 131
Sprite = J
Color = SKINCOLOR_YELLOW

Emblem
Type = Time
Var = 60*TICRATE
MapNum = 132
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 30
MapNum = 132
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = In the first trial,\\jump from the portal down to\\the pillars below.
Type = Global
Tag = 0
MapNum = 132
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = The second trial,\\after using the third spring,\\you can jump to it.
Type = Global
Tag = 1
MapNum = 132
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = In the third trial,\\use the final springs to get\\on to the pillars.
Type = Global
Tag = 2
MapNum = 132
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Type = Time
Var = 100*TICRATE
MapNum = 133
Sprite = T
Color = SKINCOLOR_GREY

Emblem
Type = Rings
Var = 30
MapNum = 133
Sprite = R
Color = SKINCOLOR_GOLD

Emblem
Hint = Springs give extra height\\when you are under water.\\Careful not to fall!
Type = Global
Tag = 0
MapNum = 133
Sprite = A
Color = SKINCOLOR_BLUE

Emblem
Hint = Bring another nuke,\\you might end up needing it\\for another switch.
Type = Global
Tag = 1
MapNum = 133
Sprite = B
Color = SKINCOLOR_RED

Emblem
Hint = The only way in\\is a well-timed double jump.\\Watch out for the blocks!
Type = Global
Tag = 2
MapNum = 133
Sprite = C
Color = SKINCOLOR_EMERALD

Emblem
Hint = Red springs at the back,\\then use the rising platforms.\\The last mystic shrine.
Type = Global
Tag = 3
MapNum = 133
Sprite = I
Color = SKINCOLOR_BLACK

Emblem
Hint = Well, this is the end.\\The door to the Mystic Realm.\\What might lay beyond?
Type = Global
Tag = 4
MapNum = 133
Sprite = H
Color = SKINCOLOR_YELLOW

Emblem
Type = Score
Var = 10000
MapNum = 135
Sprite = S
Color = SKINCOLOR_BROWN

Emblem
Type = Time
Var = 160*TICRATE
MapNum = 135
Sprite = T
Color = SKINCOLOR_GREY

