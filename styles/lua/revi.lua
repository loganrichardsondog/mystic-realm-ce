//reveries v2
//product of katsy

if VERSION != 202
	error("This mod only supports SRB2 version 2.2.", 0)
elseif SUBVERSION < 9
	error("Your copy of 2.2 is obsolete and not compatible with this mod. Please update to version 2.2.9 or higher.", 0)
end

//soc

local function SafeFreeslot(...)
	for _, item in ipairs({...})
		if rawget(_G, item) == nil
			freeslot(item)
		end
	end
end

SafeFreeslot(
	"SKINCOLOR_CRYSTAL",
	"SKINCOLOR_HAZE",
	"SKINCOLOR_OCEAN",
	"SKINCOLOR_GLEAM",
	"SKINCOLOR_SPARKLING",
	"SKINCOLOR_BREEZE",
	"SKINCOLOR_CARMINE",
	"SKINCOLOR_THORN",
	"SKINCOLOR_RADIANT",
	"SKINCOLOR_GRAPE",
	"SKINCOLOR_NOCTURNE",
	"SKINCOLOR_LAKE",
	"SKINCOLOR_PERIWINKLE",
	"SKINCOLOR_ANTICHROME",
	"SKINCOLOR_SUPERMAGENTA1",
	"SKINCOLOR_SUPERMAGENTA2",
	"SKINCOLOR_SUPERMAGENTA3",
	"SKINCOLOR_SUPERMAGENTA4",
	"SKINCOLOR_SUPERMAGENTA5",
	"MT_REVI_WHRT",
	"MT_REVI_OHRT",
	"MT_REVI_OVER",
	"MT_REVI_TORB",
	"MT_REVI_THORN",
	"S_REVI_SMOKE",
	"S_REVI_RUN",
	"S_REVI_DROPDASH",
	"S_REVI_TAILS",
	"S_REVI_GLIDE_LANDING",
	"S_REVI_CHARGE",
	"S_REVI_VAULT",
	"S_REVI_CYCLONE",
	//"S_REVI_AIRFIRE",
	"S_REVI_OVER",
	"S_REVI_TORB",
	"S_REVI_STABS",
	"S_REVI_THORN1",
	"S_REVI_THORN2",
	"SPR_THRN",
	"SPR2_DRPD",
	"SPR2_TWSP",
//	"SPR2_FAIR",
	"sfx_redrop",
	"sfx_tswtch",
	"sfx_kc65re")

skincolors[SKINCOLOR_CRYSTAL] = {
    name = "Crystal",
    ramp = {144,145,146,147,147,148,148,149,149,165,165,166,166,167,167,168},
    invcolor = SKINCOLOR_HAZE,
    invshade = 7,
    chatcolor = V_BLUEMAP,
    accessible = true}

skincolors[SKINCOLOR_HAZE] = {
    name = "Haze",
    ramp = {88,188,188,189,189,190,190,191,191,18,20,22,24,26,28,30},
    invcolor = SKINCOLOR_CRYSTAL,
    invshade = 7,
    chatcolor = V_PERIDOTMAP,
    accessible = true}
	
	skincolors[SKINCOLOR_OCEAN] = {
    name = "Ocean",
    ramp = {130,131,132,133,134,134,135,135,136,136,137,137,138,138,139,139},
    invcolor = SKINCOLOR_GLEAM,
    invshade = 6,
    chatcolor = V_SKYMAP,
    accessible = true}

	skincolors[SKINCOLOR_GLEAM] = {
    name = "Gleam",
    ramp = {0,48,49,50,52,54,56,58,60,70,70,71,71,46,46,47},
    invcolor = SKINCOLOR_OCEAN,
    invshade = 6,
    chatcolor = V_ORANGEMAP,
    accessible = true}
	
	skincolors[SKINCOLOR_SPARKLING] = {
    name = "Sparkling",
    ramp = {252,176,176,177,177,178,178,179,179,180,180,181,181,182,182,183},
    invcolor = SKINCOLOR_BREEZE,
    invshade = 5,
    chatcolor = V_MAGENTAMAP,
    accessible = true}

skincolors[SKINCOLOR_BREEZE] = {
    name = "Breeze",
    ramp = {120,121,122,123,123,141,141,142,135,136,136,137,137,138,138,139},
    invcolor = SKINCOLOR_SPARKLING,
    invshade = 9,
    chatcolor = V_AQUAMAP,
    accessible = true}
	
	skincolors[SKINCOLOR_CARMINE] = {
    name = "Carmine",
    ramp = {33,34,36,38,40,41,42,43,44,44,45,45,46,46,47,47},
    invcolor = SKINCOLOR_THORN,
    invshade = 7,
    chatcolor = V_REDMAP,
    accessible = true}
	
skincolors[SKINCOLOR_THORN] = {
    name = "Thorn",
    ramp = {89,90,91,91,92,92,93,93,94,94,95,95,109,109,110,110},
    invcolor = SKINCOLOR_CARMINE,
    invshade = 6,
    chatcolor = V_GREENMAP,
    accessible = true}
	
skincolors[SKINCOLOR_RADIANT] = {
    name = "Radiant",
    ramp = {80,81,82,83,72,73,64,64,74,65,66,67,68,69,70,71},
    invcolor = SKINCOLOR_GRAPE,
    invshade = 5,
    chatcolor = V_YELLOWMAP,
    accessible = true}
	
	skincolors[SKINCOLOR_GRAPE] = {
    name = "Grape",
    ramp = {160,161,162,162,163,163,195,195,196,196,197,197,198,198,199,199},
    invcolor = SKINCOLOR_RADIANT,
    invshade = 4,
    chatcolor = V_PURPLEMAP,
    accessible = true}
	
	skincolors[SKINCOLOR_NOCTURNE] = {
    name = "Nocturne",
    ramp = {170,171,171,172,172,173,173,174,174,175,175,159,253,253,254,254},
    invcolor = SKINCOLOR_LAKE,
    invshade = 8,
    chatcolor = V_AZUREMAP,
    accessible = true}

skincolors[SKINCOLOR_LAKE] = {
    name = "Lake",
    ramp = {202,203,204,205,205,206,206,207,207,44,44,45,45,46,46,47},
    invcolor = SKINCOLOR_NOCTURNE,
    invshade = 8,
    chatcolor = V_ROSYMAP,
    accessible = true}
	
	skincolors[SKINCOLOR_PERIWINKLE] = {
    name = "Periwinkle",
    ramp = {0,144,145,146,147,147,148,148,149,149,150,150,151,151,152,152},
    invcolor = SKINCOLOR_ANTICHROME,
    invshade = 11,
    chatcolor = V_BLUEMAP,
    accessible = true}
	
	skincolors[SKINCOLOR_ANTICHROME] = {
    name = "Antichrome",
    ramp = {30,28,26,24,22,20,18,16,14,12,10,8,6,4,2,0},
    invcolor = SKINCOLOR_PERIWINKLE,
    invshade = 3,
    chatcolor = V_INVERTMAP,
    accessible = true}
	
skincolors[SKINCOLOR_SUPERMAGENTA1] = {
    name = "Super Magenta 1",
    ramp = {0,0,0,0,0,0,0,0,0,0,200,200,176,176,177,178},
    accessible = false}

skincolors[SKINCOLOR_SUPERMAGENTA2] = {
    name = "Super Magenta 2",
    ramp = {0,200,176,176,177,177,178,178,179,179,179,179,180,180,181,181},
    accessible = false}

skincolors[SKINCOLOR_SUPERMAGENTA3] = {
    name = "Super Magenta 3",
    ramp = {176,176,177,177,178,178,179,179,179,179,180,180,181,181,182,182},
    accessible = false}

skincolors[SKINCOLOR_SUPERMAGENTA4] = {
    name = "Super Magenta 4",
    ramp = {177,178,178,179,179,179,179,180,180,181,181,182,182,183,184,185},
    accessible = false}

skincolors[SKINCOLOR_SUPERMAGENTA5] = {
    name = "Super Magenta 5",
    ramp = {178,178,179,179,179,179,180,180,181,181,182,182,183,184,185,186},
    accessible = false}

sfxinfo[sfx_shield].caption = "Shield"
//sfxinfo[sfx_armasg].caption = "Thorn Shield"
sfxinfo[sfx_s3kcfs].caption = "Elemental rise"
sfxinfo[sfx_kc65].caption = "Silence"
sfxinfo[sfx_kc65re].caption = "Power down"
	
mobjinfo[MT_REVI_WHRT] = {
	doomednum = -1,
	spawnhealth = 1,
	spawnstate = S_LHRT,
	deathstate = S_SPRK1,
	speed = 60*FRACUNIT,
	radius = 16*FRACUNIT,
	height = 16*FRACUNIT,
	flags = MF_NOGRAVITY|MF_NOBLOCKMAP|MF_MISSILE}
	
mobjinfo[MT_REVI_OVER] = {
	doomednum = -1,
	spawnhealth = 1,
	spawnstate = S_REVI_OVER,
	radius = 48*FRACUNIT,
	height = 48*FRACUNIT,
	flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT}
	
mobjinfo[MT_REVI_TORB] = {
	doomednum = -1,
	spawnhealth = 1000,
	spawnstate = S_REVI_TORB,
	speed = SH_THORN,
	radius = 48*FRACUNIT,
	height = 48*FRACUNIT,
	flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_NOGRAVITY|MF_SCENERY,
	dispoffset = 1}
	
mobjinfo[MT_REVI_THORN] = {
	doomednum = -1,
	spawnhealth = 1,
	spawnstate = S_REVI_THORN1,
	deathstate = S_NULL,
	speed = 40*FRACUNIT,
	radius = 32*FRACUNIT,
	height = 32*FRACUNIT,
	mass = DMG_SPIKE,
	flags = MF_NOGRAVITY|MF_NOBLOCKMAP|MF_MISSILE}
	
//mobjinfo[MT_PITY_ICON].seesound = sfx_armasg
mobjinfo[MT_ARMAGEDDON_ICON].seesound = sfx_bkpoof

states[S_PLAY_PAIN] = {SPR_PLAY, SPR2_PAIN, 140, NULL, 0,  0, S_PLAY_FALL}
states[S_PLAY_STUN] = {SPR_PLAY, SPR2_STUN, 140, NULL, 0,  0, S_PLAY_FALL}
states[S_PLAY_MELEE_LANDING] = {SPR_PLAY, SPR2_MLEL, 140, NULL, 0,  0, S_PLAY_FALL}
states[S_PLAY_FIRE_FINISH] = {SPR_PLAY, SPR2_FIRE, 3, NULL, S_PLAY_STND, 0, S_PLAY_STND}
states[S_PLAY_GLIDE_LANDING] = {SPR_PLAY, SPR2_LAND, 7, NULL, 0,  0, S_PLAY_STND}
states[S_REVI_RUN] = {SPR_PLAY, SPR2_RUN|FF_ANIMATE, -1, NULL, 4, 2, S_REVI_RUN}
states[S_REVI_TAILS] = {SPR_PLAY, SPR2_TWSP, -1, NULL, 0,  0, S_PLAY_FALL}
states[S_REVI_GLIDE_LANDING] = {SPR_PLAY, SPR2_LAND, 7, NULL, 0,  0, S_PLAY_STND}
states[S_REVI_CHARGE] = {SPR_METL, 18, -1, NULL, 0, 0, S_PLAY_STND}
states[S_REVI_VAULT] = {SPR_PLAY, SPR2_ROLL, 2, NULL, 0,  0, S_REVI_VAULT}
states[S_REVI_CYCLONE] = {SPR_PLAY, SPR2_MLEL, -1, NULL, 0,  0, S_PLAY_FALL}
states[S_REVI_OVER] = {SPR_ARMA, 0|FF_FULLBRIGHT|FF_ANIMATE|TR_TRANS80, -1, NULL, 15, 2, S_NULL}
states[S_REVI_SMOKE] = {SPR_NULL, 0, -1, NULL, 0, 0, S_NULL}
//states[S_REVI_TORB] = {SPR_PITY, 0|FF_ANIMATE|TR_TRANS30, -1, NULL, 11, 2, S_NULL}
states[S_REVI_STABS] = {SPR_THRN, 0|TR_TRANS40, -1, NULL, 0, 0, S_NULL}
states[S_REVI_THORN1] = {SPR_THRN, 1, -1, NULL, 0, 0, S_NULL}
states[S_REVI_THORN2] = {SPR_THRN, 2, -1, NULL, 0, 0, S_NULL}
//states[S_PITY_ICON2] = {SPR_TVPI, 2, 18, A_GiveShield, SH_ARMAGEDDON|SH_PROTECTSPIKE, 0, S_NULL}
states[S_ARMAGEDDON_ICON2] = {SPR_TVAR, 2, 18, NULL, 0, 0, S_NULL}

//globals

rawset(_G, "SH_THORN", SH_ARMAGEDDON|SH_PROTECTSPIKE)

function P_SetObjectMomZ(mo, value, relative)
	if (mo.eflags & MFE_VERTICALFLIP)
		value = -value
	end
	value = FixedMul(value, mo.scale)
	if (relative == true)
		mo.momz = $+value
	else
		mo.momz = value	
	end
	if (mo.standingslope) and (mo.player) /*and not (mo.standingslope.flags & SL_NOPHYSICS)*/ and P_IsObjectOnGround(mo) and not mo.player.powers[pw_tailsfly]
		mo.momz = $/2
	end
end

/*function A_ZThrust(actor, var1, var2)
	if (var2 & 65535)
		actor.momy = 0
		actor.momx = 0
	end
	P_SetObjectMomZ(actor, var1*FRACUNIT, !(var2>>16))
end*/

//functions

local function isplayer(mo)
	if mo.player.revi
		return true
	else
		return false
	end
end

local function newsoup(player)
	if All7Emeralds(emeralds) and (player.rings > 49) and (player.playerstate == PST_LIVE) and not (player.powers[pw_super]) and not (player.pflags & PF_SPINNING) and not (player.powers[pw_carry]) and not (player.pflags & PF_THOKKED) and not (maptol & TOL_NIGHTS)
	and not (player.panim == PA_ABILITY) and not (player.panim == PA_ABILITY2) and not (player.panim == PA_PAIN) and not ((player.panim == PA_ETC) and not (player.mo.state == S_REVI_RUN))
		return true
	else
		return false
	end
end
/*
local function stupidbouncesectors(mobj, sector)
	for fof in sector.ffloors()
		if GetSecSpecial(fof.master.frontsector.special, 1) != 15
			continue
		end
		if not (fof.flags & FF_EXISTS)
			continue
		end
		if (mobj.z+mobj.momz+FixedMul(64*FRACUNIT, mobj.scale) < fof.bottomheight) or (mobj.z-mobj.momz-FixedMul(32*FRACUNIT, mobj.scale) > fof.topheight)
			continue
		end
		return true
	end
end*/

local function revispawn(player)
	if not (player.revi)
		local revi = {
		fall = false,
		spring = 0,
		//jump = 0,
		skin = 0,
		color = 0,
		stomp = 0,
		fire = false,
		force = 0,
		y = 0,
		x = 0,
		z = 0,
		speed = 0,
		glide = false,
		//hammer = 0,
		//vault = false,
		drop = false,
		comet = 0,
		wall = 0,
		tails = 0,
		thok = 0,
		angle = 0,
		bounce = false,
		reset = false,
		charge = 0,
		over = 0,
		burn = 0}
		player.revi = revi
	end
	/*if (player.jumpfactor)  -- buffed jump height
		if (skins[player.skin].jumpfactor < FRACUNIT)
			player.revi.jump = skins[player.skin].jumpfactor+(FRACUNIT*108/1000)
		elseif (skins[player.skin].jumpfactor > FRACUNIT)
			player.revi.jump = skins[player.skin].jumpfactor+(FRACUNIT*31/100)
		else
			player.revi.jump = skins[player.skin].jumpfactor+(FRACUNIT*3/10)
		end
	else
		player.revi.jump = 0
	end*/
	if (player.charability2 == CA2_SPINDASH) and (player.mindash < 10*FRACUNIT)
		player.mindash = 10*FRACUNIT
	end
	if (player.thrustfactor > 4)
		player.acceleration = skins[player.skin].acceleration-5
	elseif (player.thrustfactor < 4)
		player.acceleration = skins[player.skin].acceleration+5
	end
	if (player.runspeed)
		player.runspeed = skins[player.skin].runspeed+(6*FRACUNIT)
	end
	if (skins[player.skin].height > 47*FRACUNIT) and (skins[player.skin].shieldscale < FRACUNIT*21/20)
		player.shieldscale = FRACUNIT*21/20
	end
	player.revi.skin = player.skin
	player.revi.color = player.skincolor
	player.revi.stomp = 0
	if (player.revi.reset)
		player.charflags = $ & ~SF_NOJUMPSPIN & ~SF_NOJUMPDAMAGE & ~SF_NOSHIELDABILITY
		player.charability2 = CA2_SPINDASH
		player.revi.reset = false
	end
end

local function revireset(player)
	player.revi.fall = false
	player.revi.force = 0
	player.revi.comet = 0
	player.revi.wall = 0
	player.revi.fire = false
	player.revi.tails = 0
	if not (player.realmo.eflags & MFE_JUSTHITFLOOR)
		player.revi.stomp = 0
		player.revi.drop = false
	end
	if (player.revi.thok)
		player.revi.thok = 0
		if (player.revi.speed)
			P_InstaThrust(player.mo, R_PointToAngle2(0, 0, player.rmomx, player.rmomy), player.revi.speed)
		end
	end
	if (player.realmo.state == S_PLAY_DASH)
		player.realmo.state = S_PLAY_RUN
	end
	player.revi.speed = 0
end

//main
			
addHook("PlayerSpawn", function(player)
	revispawn(player)
	revireset(player)
end)

addHook("PlayerThink", function(player)
	if not (player.mo)
		return
	end
	
	if not player.powers[pw_carry]
		player.speed = FixedHypot(player.rmomx, player.rmomy)
	end
	if not (player.revi) or (player.revi.skin != player.skin) or (player.revi.color != player.skincolor)
		revispawn(player)
	end
//	if (player.jumpfactor != player.revi.jump)
//		player.jumpfactor = player.revi.jump
//	end
	if YouAreSamus and YouAreSamus(player.mo)
		return
	end
//	if (player.pflags & PF_SPINNING) and not (player.pflags & PF_THOKKED) and not (player.powers[pw_justsprung])
//		player.mo.movefactor = $*3
//	end
/*	if not P_IsObjectOnGround(player.mo) and not (player.cmd.forwardmove == 0 and player.cmd.sidemove == 0) and not ((player.pflags & PF_SPINNING) and not (player.pflags & PF_THOKKED)) and not (player.pflags & PF_GLIDING)
	and not (player.powers[pw_tailsfly]) and not (player.pflags & PF_BOUNCING) and not (player.mo.state == S_REVI_CYCLONE) and not (player.powers[pw_justsprung]) and not (player.powers[pw_carry])
		player.mo.movefactor = $*2
		if not (player.speed > FixedMul(player.runspeed, player.mo.scale))
			player.mo.momy = $*49/50
			player.mo.momx = $*49/50
		elseif (player.powers[pw_super] or player.powers[pw_sneakers]) or not (player.speed > FixedMul(player.normalspeed, player.mo.scale))
			player.mo.momy = $*24/25
			player.mo.momx = $*24/25
		end
	end
	if (player.pflags & PF_STARTDASH)
		if player.powers[pw_super]
			player.dashspeed = player.maxdash
		elseif (player.dashspeed < player.maxdash)
			player.dashspeed = $ + FRACUNIT/2
		end
	end*/
	if (player.mo.eflags & MFE_SPRUNG) or P_PlayerInPain(player) or (player.playerstate != PST_LIVE) or player.powers[pw_carry]
		revireset(player)
	end
//	if (player.powers[pw_justlaunched]) and not (player.revi.spring) and not P_PlayerInPain(player)
//		if (player.powers[pw_justlaunched] == 1)
//			player.mo.momz = $*2
//		elseif (player.powers[pw_justlaunched] == 2)
//			player.mo.momz = $*3/2
//		end
//		if (player.pflags & PF_SPINNING)
//			player.pflags = $|PF_THOKKED
//		end
/*	end
	if (P_MobjFlip(player.mo)*player.mo.momz > FixedMul(5*FRACUNIT, player.mo.scale)) and not P_IsObjectOnGround(player.mo) 
		player.revi.fall = true
	end
	if (player.revi.fall) and (P_MobjFlip(player.mo)*player.mo.momz < 0) and ((player.panim == PA_WALK) or (player.panim == PA_RUN) or (player.panim == PA_DASH) or (player.mo.state == S_REVI_RUN)) and not (player.pflags & PF_JUMPED) and not player.powers[pw_carry]
		player.mo.state = S_PLAY_FALL
	end*/
	if (player.revi.spring)
		if (player.revi.spring == 1) and (P_MobjFlip(player.mo)*player.mo.momz < 0)
			player.revi.spring = 0
		end
		if (player.revi.spring == 2) and not (player.panim == PA_ABILITY) and not (player.panim == PA_ABILITY2) and not (player.panim == PA_ETC)
			player.revi.spring = 0
		end
		if P_IsObjectOnGround(player.mo) or P_PlayerInPain(player) or (player.playerstate != PST_LIVE)
			player.revi.spring = 0
		end
	end
	if (player.mo.eflags & MFE_SPRUNG) or (player.powers[pw_carry] == CR_MACESPIN) or (player.powers[pw_carry] == CR_GENERIC)
		if (P_MobjFlip(player.mo)*player.mo.momz > 0)
			player.revi.spring = 1
		else
			player.revi.spring = 3
		end
	end
	if (player.powers[pw_carry] == CR_DUSTDEVIL)
		player.revi.spring = 3
	end
	/*if stupidbouncesectors(player.mo, player.mo.subsector.sector) and not (player.revi.spring)
		player.revi.spring = 1
		if (SUBVERSION < 10) and (player.mo.skin == "amy")
			P_SetObjectMomZ(player.mo, 3*FRACUNIT, true)
		end
	end*/
//	if not (P_IsObjectOnGround(player.mo)) and not (player.revi.spring) and not player.powers[pw_carry] and not stupidbouncesectors(player.mo, player.mo.subsector.sector)
//			player.mo.momz = $+FixedMul(P_GetMobjGravity(player.mo), (abs(FixedSqrt(abs(player.mo.momz))/10)))
//		if (player.mo.standingslope) /*and not (player.mo.standingslope.flags & SL_NOPHYSICS)*/ and not P_PlayerInPain(player) and not (player.mo.state == S_PLAY_BOUNCE_LANDING)
//			player.mo.momz = $/2
//		end
//	end
//	if (player.cmd.buttons & BT_ATTACK) and (skins[player.skin].flags & SF_SUPER) and newsoup(player)
//		player.charflags = $|SF_SUPER
//		P_DoSuperTransformation(player)
//	elseif (player.charflags & SF_SUPER) and not player.powers[pw_super] and not (player.bot) and not (maptol & TOL_NIGHTS)
//		player.charflags = $ & ~SF_SUPER
//	end
/*	if (player.charability == CA_BOUNCE)
		if (player.pflags & PF_BOUNCING)
			if (player.mo.state == S_PLAY_BOUNCE_LANDING)
				P_SetObjectMomZ(player.mo, FRACUNIT/2, true)
			else
				player.mo.movefactor = $*9/5
			end
			player.revi.x = player.mo.momx
			player.revi.y = player.mo.momy
			player.revi.z = player.mo.momz
			player.revi.speed = 1
		elseif (player.revi.speed == 1) and not P_PlayerInPain(player) and not (player.playerstate == PST_DEAD) and not player.powers[pw_carry]
			player.mo.momx = player.revi.x
			player.mo.momy = player.revi.y
			player.mo.momz = player.revi.z
			player.revi.speed = 0
		end
	end
	if (player.charability == CA_GLIDEANDCLIMB) and (player.playerstate == PST_LIVE) and not P_PlayerInPain(player)
		if (player.revi.glide) and ((player.pflags & PF_STARTJUMP) or (player.mo.eflags & MFE_APPLYPMOMZ))
			player.revi.glide = false
		end
		if (player.pflags & PF_THOKKED) and (player.mo.state == S_PLAY_FALL) and not (player.pflags & PF_SHIELDABILITY)
			player.revi.x = player.rmomx/2
			player.revi.y = player.rmomy/2
			player.revi.glide = true
			if (player.speed > FixedMul(player.actionspd, player.mo.scale))
				player.mo.momy = $*47/50
				player.mo.momx = $*47/50
			end
		end
		if (player.mo.state == S_PLAY_GLIDE) and (player.mo.eflags & MFE_JUSTHITFLOOR)
			if (player.pflags & PF_SPINDOWN)
				player.skidtime = 0
				P_ResetPlayer(player)
				player.mo.state = S_PLAY_ROLL
				player.pflags = $|PF_SPINNING
				S_StartSound(player.mo, sfx_spin)
			end
		end
		if (player.mo.state == S_PLAY_GLIDE_LANDING) and not (player.mo.eflags & MFE_JUSTHITFLOOR) and (player.revi.glide) and not (player.mo.eflags & MFE_APPLYPMOMZ)
			player.mo.state = S_REVI_GLIDE_LANDING
			player.mo.momx = player.revi.x
			player.mo.momy = player.revi.y
		end
		if (player.mo.state == S_PLAY_CLIMB)
			if (player.cmd.forwardmove != 0)
				player.mo.momz = $*6/5
			end
			if (player.cmd.sidemove != 0)
				player.mo.momy = $*7/5
				player.mo.momx = $*7/5
			end
		end
		if player.powers[pw_super]
			player.charflags = $|SF_MULTIABILITY
		elseif (player.charflags & SF_MULTIABILITY) and not (skins[player.skin].flags & SF_MULTIABILITY)
			player.charflags = $ & ~SF_MULTIABILITY
		end
	end
	if (skins[player.skin].flags & SF_NOJUMPSPIN) and ((player.panim == PA_SPRING) or (player.panim == PA_FALL)) and not (player.pflags & PF_JUMPED) and not P_IsObjectOnGround(player.mo) and not player.powers[pw_carry]
		player.pflags = $|P_GetJumpFlags(player)
	end*/
end)

//damage
/*
addHook("ShouldDamage", function(sonic, inflictor, source, damage, element)
	if (inflictor) and (sonic.player)
		 if (sonic.player.powers[pw_shield] == SH_THORN)
			if (source) and (source == inflictor) and not (source.flags & MF_MISSILE)
				P_DamageMobj(source, sonic, sonic, 62, DMG_SPIKE)
				if (source.info.spawnhealth < 2) and (element == 0)
					return false
				end
			end
		end
		if ((sonic.player.powers[pw_shield] == SH_ATTRACT) and (sonic.player.homing)) or (sonic.player.revi.over) or ((sonic.skin == "amy") and ((sonic.player.panim == PA_ABILITY) or (sonic.player.panim == PA_ABILITY2) or (sonic.state == S_REVI_CYCLONE)))
			return false
		end
		if (sonic.player.powers[pw_shield] == SH_WHIRLWIND) and (inflictor.flags & MF_MISSILE) and (element == 0) and shieldtoggle == true
			local stop = P_SpawnMobjFromMobj(inflictor, 0, 0, 0, MT_DUST)
			stop.scale = $*2
			stop.color = SKINCOLOR_WHITE
			stop.colorized = true
			stop.flags2 = MF2_SHADOW
			P_SetObjectMomZ(stop, 2*FRACUNIT, false)
			S_StartSound(sonic, sfx_prloop)
			pcall(P_RemoveMobj, inflictor)
			return false
		end
	end
	if (sonic.valid) and (sonic.flags & MF_SHOOTABLE) and (inflictor) and (inflictor.valid) and (inflictor.type == MT_CORK) //cork defense
		if (inflictor.supershot)
			return true
		end
		if (sonic.flags & MF_BOSS) or (sonic.info.spawnhealth > 1)
			if not (sonic.corkshot)
				sonic.corkshot = 1
				return false
			elseif (sonic.corkshot == 1) and (sonic.flags & MF_BOSS)
				sonic.corkshot = 2
				return false
			else
				sonic.corkshot = nil
				return true
			end
		end
	end
	if (inflictor) and (inflictor.valid) and ((inflictor.type == MT_LHRT) or (inflictor.type == MT_REVI_WHRT)) and (sonic.valid) and (sonic.flags & (MF_ENEMY|MF_BOSS))
		if (inflictor.target) and (inflictor.target.player) and inflictor.target.player.powers[pw_super] and P_PlayerCanDamage(inflictor.target.player, sonic)
			if (sonic.info.spawnhealth > 0) and not (sonic.rang)
				sonic.rang = true
				S_StartSound(inflictor.target, sfx_itemup)
				P_GivePlayerRings(inflictor.target.player, 5*sonic.info.spawnhealth)
			end
		end
	end
end)*/

addHook("MobjDamage", function(force)	
	if (force.player) and (force.player.powers[pw_shield] & SH_FORCE)
		if (force.player.powers[pw_shield] & SH_FORCEHP)
			S_StartSound(force, sfx_frcssg)
		else
			S_StartSound(force, sfx_shldls)
		end
		P_RemoveShield(force.player)
		force.player.powers[pw_flashing] = flashingtics-1
		return true
	end
end, MT_PLAYER)


//specials

addHook("ShieldSpecial", function(player)	
	if (player.pflags & PF_SPINDOWN)
		return true
	end
	if (player.powers[pw_shield] == SH_THORN)
		player.pflags = $ & ~PF_STARTJUMP
		S_StartSound(player.mo, sfx_tswtch)
		for t = 1, 9
			local snarb = P_SpawnMobjFromMobj(player.mo, 0, 0, 0, MT_REVI_THORN)
			snarb.target = player.mo
			if (t < 9)
				P_InstaThrust(snarb, t*ANGLE_45, FixedMul(7*FRACUNIT, snarb.scale))
			end
			if (player.mo.momz < 0)
				if not (player.mo.eflags & MFE_VERTICALFLIP)
					snarb.state = S_REVI_THORN2
				end
				snarb.momz = FixedMul(-9*FRACUNIT, snarb.scale)
			else
				if (player.mo.eflags & MFE_VERTICALFLIP)
					snarb.state = S_REVI_THORN2
				end
				snarb.momz = FixedMul(9*FRACUNIT, snarb.scale)
			end
		end
		player.mo.momz = -$
		player.pflags = $|PF_THOKKED|PF_SHIELDABILITY
		return true
	end
	if ((player.powers[pw_shield] == SH_WHIRLWIND) or (player.powers[pw_shield] == SH_THUNDERCOIN))
		player.revi.z = player.mo.momz
		P_DoJumpShield(player)
		player.mo.momz = player.revi.z
		if (player.mo.eflags & MFE_UNDERWATER)
			P_SetObjectMomZ(player.mo, max(player.mo.momz*11/10, 6*player.jumpfactor), false)
		else
			P_SetObjectMomZ(player.mo, max(player.mo.momz*6/5, 9*player.jumpfactor), false)
		end
		return true
	end
	if (player.powers[pw_shield] == SH_FLAMEAURA)
		P_Thrust(player.mo, player.mo.angle, max(FixedMul(46*FRACUNIT, player.mo.scale)-player.speed, FixedMul(46*FRACUNIT - FixedSqrt(FixedDiv(player.speed*20, player.mo.scale)), player.mo.scale)))
		player.mo.state = S_PLAY_ROLL
		player.pflags = $|PF_THOKKED|PF_SHIELDABILITY & ~PF_NOJUMPDAMAGE
		if mariomode
			S_StartSound(player.mo, sfx_mario7)
		else
			S_StartSound(player.mo, sfx_s3k43)
		end
		return true
	end
	if (player.powers[pw_shield] & SH_FORCE)
		S_StartSound(player.mo, sfx_ngskid)
		player.pflags = $|PF_THOKKED|PF_SHIELDABILITY & ~PF_STARTJUMP
		player.revi.force = FixedHypot(player.rmomx, player.rmomy)+1
		return true
	end
	if (player.powers[pw_shield] == SH_ELEMENTAL) or (player.powers[pw_shield] == SH_BUBBLEWRAP)
		player.pflags = $|PF_THOKKED & ~PF_STARTJUMP & ~PF_NOJUMPDAMAGE & ~PF_SPINNING
		player.secondjump = 0
		if (player.powers[pw_shield] == SH_BUBBLEWRAP)
			player.revi.stomp = 2
			player.pflags = $|PF_SHIELDABILITY
			player.mo.state = S_PLAY_ROLL
			P_SetObjectMomZ(player.mo, min(player.mo.momz*7/5, -24*FRACUNIT), false)
		else
			player.revi.fire = true
			if (player.mo.eflags & MFE_UNDERWATER)
				P_SetObjectMomZ(player.mo, max(player.mo.momz, 3*FRACUNIT), false)
			else
				P_SetObjectMomZ(player.mo, max(player.mo.momz, 6*FRACUNIT), false)
			end
			player.mo.state = S_PLAY_SPRING
		end
		return true
	end
end)
/*
addHook("SpinSpecial", function(player)
	if ((player.cmd.buttons & BT_JUMP) and not (player.pflags & PF_JUMPDOWN))
		return true
	end
	if (player.pflags & PF_SPINDOWN)
		return false
	end
	if (player.pflags & PF_SPINNING) and P_IsObjectOnGround(player.mo) and not (player.pflags & PF_STARTDASH)
		player.mo.state = S_PLAY_WALK
		player.pflags = $ & ~PF_SPINNING
		player.pflags = $|PF_SPINDOWN
		return true
	end
	if not P_IsObjectOnGround(player.mo) and not (player.pflags & PF_JUMPED) and not P_PlayerInPain(player) and not (player.pflags & PF_SPINNING) and not player.powers[pw_carry]
		if (player.revi.over) and not (player.weapondelay)
			player.pflags = $|PF_JUMPED|PF_THOKKED
			S_StartSound(player.mo, sfx_cdfm35)
			P_InstaThrust(player.mo, player.mo.angle, max(FixedMul(46*FRACUNIT, player.mo.scale), FixedHypot(player.rmomx, player.rmomy)))
			player.mo.state = S_PLAY_DASH
			player.weapondelay = TICRATE
			return true
		end
	end
end)*/

addHook("JumpSpecial", function(player)
	if ((player.mo.eflags & MFE_JUSTHITFLOOR) or P_IsObjectOnGround(player.mo)) and not (player.pflags & PF_JUMPDOWN) and not (player.pflags & PF_FULLSTASIS) and not (player.pflags & PF_JUMPSTASIS)
		revireset(player)
		if (player.pflags & PF_SPINNING)
			player.pflags = $ & ~PF_SPINNING
		end
	end
end)

addHook("TouchSpecial", function(flower, mario)	
	if (mario.player) and not (mario.player.bot)
	and not MarioSkins[mo.skin]
		S_StartSound(mario, sfx_mario3)
		P_SwitchShield(mario.player, SH_FLAMEAURA)
		P_KillMobj(flower, mario, mario)
	end
	return true
end, MT_FIREFLOWER)

addHook("ShieldSpawn", function(player)
	if (player.powers[pw_shield] == SH_THORN)
		local thorn = P_SpawnMobjFromMobj(player.mo, 0, 0, 0, MT_REVI_TORB)
		thorn.threshold = SH_THORN
		thorn.target = player.mo
		thorn.flags2 = $|MF2_SHIELD
		local stabs = P_SpawnMobjFromMobj(thorn, 0, 0, 0, MT_OVERLAY)
		stabs.state = S_REVI_STABS
		stabs.target = thorn
		stabs.spritexscale = $*11/16
		stabs.spriteyscale = $*11/16
		return true
	end
end)

addHook("PlayerHeight", function(player)
	if (player.mo.state == S_REVI_VAULT) or (player.mo.state == S_PLAY_GLIDE_LANDING)
		return P_GetPlayerHeight(player)
	end
	if (player.charflags & SF_MACHINE) and (player.panim == PA_DASH)
		return P_GetPlayerSpinHeight(player)
	end
end)
	
//miscellaneous

addHook("MobjThinker", function(shield)
	if (shield.target) and shield.target.valid
		if (shield.state == S_REVI_STABS) and not (leveltime%4)
			shield.rollangle = $+ANGLE_22h
		end
		if (shield.target.type == MT_ELEMENTAL_ORB) and (shield.target.target) and pcall(isplayer, shield.target.target)
			if ((shield.target.target.eflags & MFE_UNDERWATER or shield.target.target.eflags & MFE_TOUCHWATER) and not (shield.target.target.eflags & MFE_TOUCHLAVA)) or (shield.target.target.player.revi.fire)
				shield.flags2 = $|MF2_DONTDRAW
			elseif (shield.flags2 & MF2_DONTDRAW)
				shield.flags2 = $ & ~MF2_DONTDRAW
			end
			if (shield.target.target.player.revi.fire) and not (shield.stomping) and not ((shield.target.target.eflags & MFE_UNDERWATER) and not (shield.target.target.eflags & MFE_TOUCHLAVA))
				shield.target.state = shield.target.info.raisestate
				shield.target.rollangle = ANGLE_180
				shield.target.spriteyoffset = -24*FRACUNIT
				shield.stomping = true
			elseif (shield.stomping)
				shield.target.state = shield.target.info.spawnstate
				shield.target.rollangle = 0
				shield.target.spriteyoffset = 0
				shield.stomping = false
			end
		end
	end
end, MT_OVERLAY)

addHook("MobjThinker", function(shield)
	if not (shield.target) or not pcall(isplayer, shield.target)
		return
	end
	if P_IsObjectOnGround(shield.target) and ((shield.target.player.panim == PA_RUN) or (shield.target.player.panim == PA_DASH) or (shield.target.state == S_REVI_RUN)) and not (leveltime%2)
		P_ElementalFire(shield.target.player, false)
	end
	if (shield.target.player.revi.fire) and not (shield.rocketing)
		shield.rocketing = TICRATE
	end
	if (shield.rocketing)
		if not (leveltime%2)
			S_StartSound(shield.target, sfx_s3kcfs)
		end
		local rocketfire = P_SpawnMobjFromMobj(shield, 0, 0, 0, MT_SPINDUST)
		if (shield.target.eflags & MFE_UNDERWATER) and not (shield.target.eflags & MFE_TOUCHLAVA)
			rocketfire.state = S_SPINDUST_BUBBLE1
			P_SetObjectMomZ(shield.target, FRACUNIT/2, true)
		else
			rocketfire.state = S_SPINDUST_FIRE1
			P_SetObjectMomZ(shield.target, FRACUNIT, true)
		end
		P_InstaThrust(rocketfire, P_RandomRange(1, 360)*ANG1, FixedMul(10*FRACUNIT, rocketfire.scale))
		shield.rocketing = $-1
		shield.target.momy = 0
		shield.target.momx = 0
		if (shield.rocketing < 2) or (P_MobjFlip(shield.target)*shield.target.momz < FixedMul(3*FRACUNIT, shield.target.scale))
			S_StopSoundByID(shield.target, sfx_s3kcfl)
			shield.target.momz = 0
			revireset(shield.target.player)
			shield.rocketing = nil
			if (shield.target.eflags & MFE_UNDERWATER) and not (shield.target.eflags & MFE_TOUCHLAVA)
				A_OldRingExplode(shield.target, MT_EXTRALARGEBUBBLE)
				P_RadiusAttack(shield, shield.target, 192*FRACUNIT, DMG_WATER, true)
				S_StartSound(shield.target, sfx_s3k57)
			else
				A_OldRingExplode(shield.target, MT_FIREBALLTRAIL)
				P_RadiusAttack(shield, shield.target, 192*FRACUNIT, DMG_FIRE, true)
				S_StartSound(shield.target, sfx_s3k6c)
			end
			shield.target.player.pflags = $|PF_NOJUMPDAMAGE
			if not (shield.target.player.cmd.forwardmove == 0 and shield.target.player.cmd.sidemove == 0)
				P_InstaThrust(shield.target, (shield.target.player.cmd.angleturn<<16 + R_PointToAngle2(0, 0, shield.target.player.cmd.forwardmove*FRACUNIT, -shield.target.player.cmd.sidemove*FRACUNIT)), FixedMul(5*FRACUNIT, shield.target.scale))
			end
		end
	end
end, MT_ELEMENTAL_ORB)

addHook("MobjThinker", function(shield)
	if (shield.target) and pcall(isplayer, shield.target)
		if (shield.target.player.revi.force)
			if (shield.target.player.pflags & PF_SPINDOWN)
				shield.target.momx = 0
				shield.target.momy = 0
				shield.target.momz = 0
			else
				P_Thrust(shield.target, shield.target.angle, shield.target.player.revi.force)
				shield.target.player.revi.force = 0
			end
		end
	end
end, MT_FORCE_ORB)


addHook("MobjThinker", function(actor)
	if (actor.state == S_ARMAGEDDON_ICON2) and (actor.target) and pcall(isplayer, actor.target) and not (actor.armageddon)
		actor.armageddon = 666
		S_StartSound(actor, sfx_bkpoof)
		P_FlashPal(actor.target.player, PAL_NUKE, TICRATE)
		P_StartQuake(62*FRACUNIT, TICRATE*6/5, {actor.x, actor.y, actor.z}, FixedMul(4800*FRACUNIT, actor.scale))
		for enemy in mobjs.iterate()
			if (enemy.valid) and (enemy.health) and not (enemy.player)
				P_AddPlayerScore(actor.target.player, 1)
			else
				continue
			end
			if R_PointToDist2(actor.x, actor.y, enemy.x, enemy.y) <= FixedMul(2400*FRACUNIT, actor.scale)
				if (enemy.flags & MF_SHOOTABLE) and not (enemy.flags & MF_MONITOR)
					P_KillMobj(enemy, actor, actor)
					P_AddPlayerScore(actor.target.player, 750)
					continue
				end
			end
			if (enemy.flags & MF_ENEMY) and not (enemy.flags & MF_BOSS)
				if enemy.spawnhealth == 0
					P_KillMobj(enemy, actor, actor)
				elseif P_RandomChance(FRACUNIT/3)
					P_KillMobj(enemy, actor, actor)
					P_AddPlayerScore(actor.target.player, 350)
				end
			end
		end
	end
end, MT_ARMAGEDDON_ICON)
/*
addHook("MobjRemoved", function(thorn)
	for p = 1, 8
		local psn = P_SpawnMobjFromMobj(thorn, 0, 0, 0, MT_EXPLODE)
		psn.color = SKINCOLOR_LAKE
		psn.colorized = true
		psn.flags = MF_NOBLOCKMAP|MF_NOGRAVITY|MF_NOCLIPHEIGHT
		psn.fuse = TICRATE/2
		pcall(P_InstaThrust, psn, p*ANGLE_45, FixedMul(11*FRACUNIT, psn.scale))
		pcall(P_RadiusAttack, psn, thorn.target, 128*FRACUNIT, DMG_NUKE, true)
	end
end, MT_REVI_THORN)*/

/*addHook("MobjMoveCollide", function(metal, spike)
	if not spike.valid or not pcall(isplayer, metal)
		return nil
	end
	if ((metal.skin == "metalsonic") or (metal.state == S_REVI_CYCLONE)) and (spike.type == MT_SPIKE or spike.type == MT_WALLSPIKE)
	 	if not (spike.z > metal.z+metal.height) and not (metal.z > spike.z+spike.height)
			pcall(P_KillMobj, spike, metal, metal)
		end
		return false
	end
	if (spike.flags & MF_SOLID) and not (spike.player)
	 	if not (spike.z > metal.z+metal.height) and not (metal.z > spike.z+spike.height)
			metal.notawall = true
		end
	end
	if ((metal.player.revi.stomp == 1) or (metal.state == S_REVI_CYCLONE)) and (spike.flags & MF_SPRING) and (spike.info.painchance != 3)
	 	if not (spike.z > metal.z+metal.height) and not (metal.z > spike.z+spike.height)
			metal.player.pflags = $|PF_NOJUMPDAMAGE
			metal.state = S_PLAY_TWINSPIN
			metal.frame = 6
		end
	end
end, MT_PLAYER)*/

addHook("MobjCollide", function(steam, other)
	if not other.player
		return nil
	end
	if not (steam.z > other.z+other.height) and not (other.z > steam.z+steam.height)
		other.eflags = $|MFE_SPRUNG
	end
end, MT_STEAM)